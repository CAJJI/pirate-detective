using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ConfirmationMenu : MenuBase<ConfirmationMenu>
{
    const string PATH = "Prefabs/ConfirmationMenu";
    const string CANVAS_NAME = "UIManager";

    public Button confirmButton;
    public Button denyButton;
    public TextMeshProUGUI text;

    Action onConfirm;
    Action onDeny;

    public static void Open(string description, string confirmText, string denyText, Action onConfirm, Action onDeny = null)
    {
        ConfirmationMenu prefab = Resources.Load<ConfirmationMenu>(PATH);
        ConfirmationMenu menu = Instantiate(prefab, GetCanvas().transform);

        menu.Initialize(description, confirmText, denyText, onConfirm, onDeny);
    }

    static Canvas GetCanvas()
    {
        foreach(Canvas canvas in FindObjectsOfType<Canvas>())
        {
            if (canvas.gameObject.name == CANVAS_NAME)
                return canvas;
        }
        return null;
    }

    public void Initialize(string description, string confirmText, string denyText, Action onConfirm, Action onDeny)
    {
        text.text = description;
        confirmButton.GetComponentInChildren<TextMeshProUGUI>().text = confirmText;
        denyButton.GetComponentInChildren<TextMeshProUGUI>().text = denyText;
        this.onConfirm = onConfirm;
        this.onDeny = onDeny;

    }    

    public void Confirm()
    {
        onConfirm?.Invoke();
        Close();
    }

    public void Deny()
    {
        onDeny?.Invoke();
        Close();
    }
}
