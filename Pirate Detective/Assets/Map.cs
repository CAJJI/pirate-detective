using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map : MenuBase<Map>
{
    //if mouse over map location
    //highlight location
    //if click on location, teleport, or prevent

    // REJECT IF YOU ARE ALREADY AT LOCATION

    List<MapLocation> locations = new List<MapLocation>();

    public override void Open()
    {
        base.Open();
        
        locations.Clear();
        locations.AddRange(GetComponentsInChildren<MapLocation>());
        foreach(MapLocation location in locations)
        {
            location.UpdateState();
        }
    }
}
