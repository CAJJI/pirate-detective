﻿using UnityEngine;

public class RotateObject : MonoBehaviour
{
    Vector3 previousPos = Vector3.zero;
    Vector3 posDelta = Vector3.zero;

    void Update()
    {
        if (Input.GetMouseButton(0)) 
        {
            posDelta = Input.mousePosition - previousPos;
            //I remember camera.main has some weird allocation thing, super unimportant
            if (Vector3.Dot(transform.up, Vector3.up) >= 0)
            {
                transform.Rotate(transform.up, -Vector3.Dot(posDelta, Camera.main.transform.right), Space.World);

            }
            else 
            {
                transform.Rotate(transform.up, Vector3.Dot(posDelta, Camera.main.transform.right), Space.World);
            }
            transform.Rotate(Camera.main.transform.right, Vector3.Dot(posDelta, Camera.main.transform.up), Space.World);
        }

        previousPos = Input.mousePosition;
    }
}
