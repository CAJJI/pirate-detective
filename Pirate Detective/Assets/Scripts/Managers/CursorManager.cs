using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CursorManager : Manager
{
    public static CursorManager Instance { get; private set; }

    public GraphicRaycaster graphicsRaycaster;

    public bool IsMouseOverUI()
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        graphicsRaycaster.Raycast(eventData, results);

        if (results.Count == 0)
        {
            return false;
        }

       // Debug.Log(results[0]);
        return true;
    }

    public override void MAwake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        if (!UIManager.Instance) return;

        graphicsRaycaster = Engine.Instance.uiManager.GetComponent<GraphicRaycaster>();
    }

    public override void MStart()
    {
    }

    public override void MUpdate()
    {

        
    }
}
