using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using Rewired;

public class InputManager : Manager
{
    public static InputManager Instance { get; private set; }

    public Rewired.Player inputPlayer;
    public Vector2 MoveDirection { get; private set; }
    public Vector2 LookDirection { get; private set; }

    [HideInInspector] public UnityEvent GameplayInputEvent_PrimaryInteractDown = new UnityEvent();
    [HideInInspector] public UnityEvent GameplayInputEvent_PrimaryInteractUp = new UnityEvent();
    [HideInInspector] public UnityEvent GameplayInputEvent_PrimaryInteractHoldDown = new UnityEvent();
    [HideInInspector] public UnityEvent GameplayInputEvent_PrimaryInteractHoldUp = new UnityEvent();
    [HideInInspector] public UnityEvent GameplayInputEvent_PauseDown = new UnityEvent();

    [HideInInspector] public UnityEvent MenuInputEvent_ConfirmDown = new UnityEvent();
    [HideInInspector] public UnityEvent MenuInputEvent_ConfirmUp = new UnityEvent();
    [HideInInspector] public UnityEvent MenuInputEvent_ConfirmHoldDown = new UnityEvent();
    [HideInInspector] public UnityEvent MenuInputEvent_ConfirmHoldUp = new UnityEvent();
    [HideInInspector] public UnityEvent MenuInputEvent_CancelDown = new UnityEvent();
    [HideInInspector] public UnityEvent MenuInputEvent_UnpauseDown = new UnityEvent();

    private bool gameplayInputDisabled;

    public GameObject RewiredInputManager;

    public override void MAwake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

       // if (RewiredInputManager)

        inputPlayer = ReInput.players.GetPlayer(0);
    }
    public override void MStart() { }
    public override void MUpdate()
    {
        UpdateGameplayInput();
        UpdateMenuInput();
    }

    private void UpdateGameplayInput()
    {
        MoveDirection = Vector2.zero;

        if (gameplayInputDisabled) return;

        MoveDirection = new Vector2(inputPlayer.GetAxis(RewiredConsts.Action.MoveHorizontal), inputPlayer.GetAxis(RewiredConsts.Action.MoveForward));
        LookDirection = new Vector2(inputPlayer.GetAxis(RewiredConsts.Action.LookX), inputPlayer.GetAxis(RewiredConsts.Action.LookY));

        //Interact
        if (inputPlayer.GetButtonDown(RewiredConsts.Action.Interact))
        {
            GameplayInputEvent_PrimaryInteractDown.Invoke();
        }
        else if (inputPlayer.GetButtonUp(RewiredConsts.Action.Interact))
        {
            GameplayInputEvent_PrimaryInteractUp.Invoke();
        }

        if (inputPlayer.GetButtonLongPressDown(RewiredConsts.Action.Interact))
        {
            GameplayInputEvent_PrimaryInteractHoldDown.Invoke();
        }
        else if (inputPlayer.GetButtonLongPressUp(RewiredConsts.Action.Interact))
        {
            GameplayInputEvent_PrimaryInteractHoldUp.Invoke();
        }

        if (inputPlayer.GetButtonDown(RewiredConsts.Action.Pause))
        {
            GameplayInputEvent_PauseDown.Invoke();
        }
    }

    private void UpdateMenuInput()
    {
        if (inputPlayer.GetButtonDown(RewiredConsts.Action.Confirm))
        {
            MenuInputEvent_ConfirmDown.Invoke();
        }
        else if (inputPlayer.GetButtonUp(RewiredConsts.Action.Confirm))
        {
            MenuInputEvent_ConfirmUp.Invoke();
        }

        if (inputPlayer.GetButtonShortPressDown(RewiredConsts.Action.Confirm))
        {
            MenuInputEvent_ConfirmHoldUp.Invoke();
        }
        else if (inputPlayer.GetButtonShortPressUp(RewiredConsts.Action.Confirm))
        {
            MenuInputEvent_ConfirmHoldDown.Invoke();
        }

        if (inputPlayer.GetButtonDown(RewiredConsts.Action.Cancel))
        {
            MenuInputEvent_CancelDown.Invoke();
        }

        if (inputPlayer.GetButtonDown(RewiredConsts.Action.Unpause))
        {
            MenuInputEvent_UnpauseDown.Invoke();
        }
    }

    public void Event_OnExit()
    {

    }

    public void PauseGameplayInput(bool paused)
    {
        gameplayInputDisabled = paused;
    }

    public void SetControllerMap(params ControllerMap[] maps)
    {
        inputPlayer.controllers.maps.SetAllMapsEnabled(false);

        foreach (ControllerMap map in maps)
        {
            inputPlayer.controllers.maps.SetMapsEnabled(true, map.ToString());
        }
    }



}
