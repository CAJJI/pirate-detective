using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SceneTransitionManager : Manager
{
    public static SceneTransitionManager Instance { get; private set; }

    [HideInInspector] public UnityEvent Event_LoadData = new UnityEvent();

    public TransitionSaveData transitionData;

    public string targetScene;
    public string targetEntrance;

    bool transitioning;

    private List<SceneTransitionZone> sceneTransitionZones = new List<SceneTransitionZone>();
    public override void MAwake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public override void MStart()
    {
    }
    public override void MUpdate() { }



    public void InitializeScene()
    {
        sceneTransitionZones = new List<SceneTransitionZone>(FindObjectsOfType<SceneTransitionZone>());

        SetAllZonesActive(false);

        Vector3 playerSpawnPosition = Vector3.zero;

        GameObject playerGameobject = Instantiate(Engine.Instance.player, playerSpawnPosition, Quaternion.identity) as GameObject;
        Character playerCharacter = playerGameobject.GetComponent<Character>();

        SceneTransitionZone targetZone = null;

       


#if UNITY_EDITOR
        SceneTestDebug debugScene = FindObjectOfType<SceneTestDebug>();

        if (SaveManager.Instance.transitionData.firstLoad && debugScene && debugScene.active)
        {
            debugScene.LoadDebug();
        }
        else
#endif
        {
            SaveManager.Instance.LoadData();
        }

        if (!string.IsNullOrEmpty(targetEntrance))
        {
            targetZone = GetZoneInScene(targetEntrance);
        }
        else
        {
            targetZone = GetDefaultZone();
        }

        if (targetZone)
        {
            playerCharacter.agent.Warp(targetZone.entrance.position);
            playerCharacter.GetMovementFunction().MoveToDestination(targetZone.enterWalktarget.position, true);
            return;
        }

        playerCharacter.agent.Warp(playerSpawnPosition);
    }    

    public void SetSceneTransition(string targetSceneName, string targetEntranceName)
    {
        if (transitioning)
            return;

        targetScene = targetSceneName;
        targetEntrance = targetEntranceName;
        SaveManager.Instance.SaveData();
        transitioning = true;

        UILoadingScreen.Instance.FadeIn(0.6f, () => { SceneManager.LoadScene(targetScene); transitioning = false; });
    }

    public SceneTransitionZone GetZoneInScene(string name)
    {
        foreach (SceneTransitionZone zone in sceneTransitionZones)
        {
            if (zone.thisEntranceName == name)
            {
                return zone;
            }
        }

        Debug.LogWarning("There is no Transition Zone with the entrance " + name);
        return null;
    }

    public SceneTransitionZone GetDefaultZone()
    {
        foreach (SceneTransitionZone zone in sceneTransitionZones)
        {
            if (zone.defaultEntrance)
            {
                return zone;
            }
        }

        Debug.LogWarning("There is no Transition Zone marked as Default");
        return null;
    }

    public void SetAllZonesActive(bool isActive)
    {
        foreach (SceneTransitionZone zone in sceneTransitionZones)
        {
            zone.SetZoneActive(isActive);
        }
    }
}
