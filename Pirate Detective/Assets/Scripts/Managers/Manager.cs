using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Manager : MonoBehaviour
{
    public abstract void MAwake();
    public abstract void MStart();
    public abstract void MUpdate(); 

}
