using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EvidenceManager : Manager
{
    public static EvidenceManager Instance { get; private set; }

    [Header("Containers")]
    public EvidenceContainer evidenceContainer;

    public Dictionary<string, Evidence> evidence = new Dictionary<string, Evidence>();    

    [System.Serializable]
    public class GatheredEvidence
    {
        public UnityEvent<Evidence> onEvidenceAdded;
        public UnityEvent<Evidence> onEvidenceRemoved;

        public List<Evidence> evidence = new List<Evidence>();
        public List<Evidence> newEvidence = new List<Evidence>();
        public List<Evidence> hiddenEvidence = new List<Evidence>();

        public bool HasGathered(Evidence evidenceToCheck)
        {
            bool hasGathered = evidence.Contains(Engine.Instance.evidenceManager.evidence[evidenceToCheck.key]);
            return hasGathered;
        }

      
        public void AddEvidence(Evidence evidenceToAdd)
        {
            if (!Engine.Instance.evidenceManager.evidence.ContainsKey(evidenceToAdd.key))
            {
                Debug.LogWarning("Tried to add " + evidenceToAdd.evidenceName + " to Gathered Evidence. This evidence is not loaded.", evidenceToAdd);
                return;
            }

            if (evidence.Contains(Engine.Instance.evidenceManager.evidence[evidenceToAdd.key]))
            {
                Debug.Log(evidenceToAdd.evidenceName + " is already gathered.", evidenceToAdd);
                return;
            }

            evidence.Add(Engine.Instance.evidenceManager.evidence[evidenceToAdd.key]);
            newEvidence.Add(Engine.Instance.evidenceManager.evidence[evidenceToAdd.key]);
            onEvidenceAdded?.Invoke(evidenceToAdd);

            AudioManager.PlaySfx(AudioDef.Instance.evidenceAdded);            

            Debug.Log("Successfully added " + evidenceToAdd.evidenceName + " to Evidence Inventory", evidenceToAdd);
        }

        public void RemoveEvidence(Evidence evidenceToRemove)
        {
            if (!Engine.Instance.evidenceManager.evidence.ContainsKey(evidenceToRemove.key))
            {
                Debug.LogWarning("Tried to remove " + evidenceToRemove.evidenceName + " from Gathered Evidence. This evidence is not loaded.", evidenceToRemove);
                return;
            }

            if (!evidence.Contains(Engine.Instance.evidenceManager.evidence[evidenceToRemove.key]))
            {

                Debug.Log(evidenceToRemove.evidenceName + " is not in the player's inventory.", evidenceToRemove);
                return;
            }

            if (hiddenEvidence.Contains(Engine.Instance.evidenceManager.evidence[evidenceToRemove.key]))
            {
                Debug.Log(evidenceToRemove.evidenceName + " has already been removed from the player's inventory.", evidenceToRemove);
                return;
            }


            hiddenEvidence.Add(Engine.Instance.evidenceManager.evidence[evidenceToRemove.key]);

            if (newEvidence.Contains(Engine.Instance.evidenceManager.evidence[evidenceToRemove.key]))
            {
                newEvidence.Remove(Engine.Instance.evidenceManager.evidence[evidenceToRemove.key]);
            }

            onEvidenceRemoved?.Invoke(evidenceToRemove);

            //TODO: JESSI on remove audio?
            //AudioManager.PlaySfx(AudioDef.Instance.evidenceAdded);

            Debug.Log("Successfully removed " + evidenceToRemove.evidenceName + " from Evidence Inventory", evidenceToRemove);
        }
    }

    public GatheredEvidence gatheredEvidence = new GatheredEvidence();

    public override void MAwake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        LoadAllEvidence();
    }

    public override void MStart() {}
    public override void MUpdate() {}

    private void LoadAllEvidence()
    {
        evidence.Clear();

        foreach (Evidence evi in evidenceContainer.evidence)
        {
            evidence.Add(evi.key, evi);
        }
    }

}
