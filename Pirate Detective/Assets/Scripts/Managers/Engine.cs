using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : Singleton<Engine>
{
    public GameObject player;

    [Header("Managers")]
    public SaveManager saveManager;
    public InputManager inputManager;
    public UIManager uiManager;
    public EvidenceManager evidenceManager;
    public ObjectiveManager objectiveManager;
    public SceneTransitionManager sceneTransitionManager;
    public CursorManager cursorManager;

    private List<Manager> managers = new List<Manager>();

    public void Awake()
    {
        managers.Add(saveManager);
        managers.Add(inputManager);
        managers.Add(uiManager);
        managers.Add(evidenceManager);
        managers.Add(objectiveManager);
        managers.Add(sceneTransitionManager);
        managers.Add(cursorManager);

        foreach (Manager manager in managers)
        {
            manager.MAwake();
        }

        OnGameplayStart();
    }

    private void Start()
    {
        foreach (Manager manager in managers)
        {
            manager.MStart();
        }
    }

    public void OnGameplayStart()
    {
        sceneTransitionManager.InitializeScene();
        UILoadingScreen.Instance.FadeOut(1, () => { sceneTransitionManager.SetAllZonesActive(true); });
    }


    private void Update()
    {
        foreach (Manager manager in managers)
        {
            manager.MUpdate();
        }
    }
}
