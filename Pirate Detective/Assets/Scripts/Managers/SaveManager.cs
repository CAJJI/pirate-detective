using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : Manager
{
    public static SaveManager Instance { get; private set; }

    public TransitionSaveData transitionData;
    public ObjectiveSaveData objectiveData;
    public EvidenceSaveData evidenceData;

    public override void MAwake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void SaveData()
    {
        transitionData.Save();
        objectiveData.Save();
        evidenceData.Save();
    }

    public void LoadData()
    {
        transitionData.Load();
        objectiveData.Load();
        evidenceData.Load();
    }

    public void ClearData()
    {
        transitionData.Clear();
        evidenceData.Clear();
        objectiveData.Clear();
    }

    public override void MStart()
    {
        
    }

    public override void MUpdate()
    {
       
    }

    private void OnApplicationQuit()
    {
        ClearData();
    }

}
