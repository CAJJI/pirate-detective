using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Manager
{
    public static UIManager Instance { get; private set; }

    public CanvasScaler scaler;
    
    public UIHUD uiHUD;
    public UIDialogue uiDialogue;
    public UILoadingScreen uiLoadingScreen;
    public UIEvidenceInspection uiEvidenceInspection;
    public UILog uiLog;

    [System.NonSerialized]
    public List<UILayer> layers = new List<UILayer>();
    [System.NonSerialized]
    public Dictionary<string, UILayer> layersAccessor = new Dictionary<string, UILayer>();

    public UILayer currentLayer;
    public UILayer previousLayer;



    public override void MAwake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        layers.Clear();
        layersAccessor.Clear();

        scaler = GetComponent<CanvasScaler>();

        layers.Add(uiHUD);
        layers.Add(uiEvidenceInspection);
        layers.Add(uiDialogue);
        layers.Add(uiLoadingScreen);
        layers.Add(uiLog);

        foreach (UILayer layer in layers)
        {
            layersAccessor.Add(layer.UIKey, layer);
            layer.UI_Awake();
            layer.LoseFocus();
        }
    }

    public override void MStart()
    {
        foreach (UILayer layer in layers)
        {
            layer.UI_Start();
        }
    }

    public override void MUpdate()
{
        foreach (UILayer layer in layers)
        {
            layer.UI_Update();
        }
    }

    public void SetFocus(string layer)
    {
        if (!layersAccessor.ContainsKey(layer))
        {
            Debug.Log("\"" + layer + "\" is not a valid key for any of the UIManager's set of UILayers");
            return;
        }

        SetFocus(layersAccessor[layer]);
    }

    public void SetFocus(UILayer layer)
    {
        if (!layers.Contains(layer))
        {
            Debug.Log(layer.ToString() + " is not added to the UIManager's set of UILayers");
            return;
        }

        previousLayer = currentLayer;
        currentLayer = layer;

        if (previousLayer)
            previousLayer.LoseFocus();

        currentLayer.GainFocus();
        
    }



}
