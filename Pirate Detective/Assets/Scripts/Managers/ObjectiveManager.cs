using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveManager : Manager
{
    public static ObjectiveManager Instance { get; private set; }

    [Header("Containers")]
    public ObjectiveContainer objectiveContainer;

    public Dictionary<string, DetectiveObjective> objectives = new Dictionary<string, DetectiveObjective>();

    public override void MAwake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        LoadAllObjectives();
    }

    public override void MStart()
    {
        ObjectDeactivator[] onSceneStart = FindObjectsOfType<ObjectDeactivator>();

        foreach (ObjectDeactivator item in onSceneStart)
        {
            item.OnSceneStart();
        }
    }

    public override void MUpdate()
    {
        
    }

    private void LoadAllObjectives()
    {
        objectives.Clear();

        foreach (DetectiveObjective obj in objectiveContainer.objectives)
        {
            objectives.Add(obj.key, obj);
        }
    }

}
