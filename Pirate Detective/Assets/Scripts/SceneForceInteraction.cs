using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneForceInteraction : MonoBehaviour
{

    public Interactable interactable;

    void Start()
    {
        UILoadingScreen.Instance.Event_OnFadeOutComplete.AddListener(Event_ForceInteraction);
    }

    private void Event_ForceInteraction()
    {
        if (interactable)
            Player.Instance.GetMovementFunction().MoveToDestination(interactable, true);
    }
}
