//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;

//[CustomEditor(typeof(DialogueSegment))]
//public class DialogueSegmentEditor : Editor
//{
//    bool commentsShown = false;

//    public override void OnInspectorGUI()
//    {
//        DialogueSegment target = (DialogueSegment)this.target;

//        GUILayout.BeginVertical();

//        if (GUILayout.Button("Toggle Comments"))
//        {
//            commentsShown = !commentsShown;
//        }

//        GUILayout.BeginHorizontal();

//        if (GUILayout.Button("Expand Nodes"))
//        {
//            ExpandNodes(true);
//        }

//        if (GUILayout.Button("Collapse Nodes"))
//        {
//            ExpandNodes(false);
//        }

//        GUILayout.EndHorizontal();

//        GUILayout.EndVertical();

//        SerializedProperty comment = serializedObject.FindProperty("comment");

//        if (commentsShown)
//            comment.stringValue = EditorGUILayout.TextArea(comment.stringValue);

//        DrawDefaultInspector();
//    }

//    public void ExpandNodes(bool state)
//    {
//        SerializedProperty nodes = serializedObject.FindProperty("nodes");

//        for (int i = 0; i < nodes.arraySize; i++)
//        {
//            nodes.GetArrayElementAtIndex(i).isExpanded = state;
//        }
//    }
//}

