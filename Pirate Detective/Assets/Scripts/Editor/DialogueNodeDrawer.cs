using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(DialogueNode))]
public class DialogueNodeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        string dialogue = property.FindPropertyRelative("dialogue").stringValue;
        bool isPlayer = property.FindPropertyRelative("isPlayer").boolValue;

        string labelText = "";

        labelText += isPlayer ? "Player: " : "Participant: ";
        labelText += dialogue;

        if (labelText.Length >= 80)
        {
            labelText = labelText.Substring(0, 77);
            labelText += "...";
        }

        label.text = labelText;

        EditorGUI.PropertyField(position, property, label, true);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property);
    }

}
