using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTransitionZone : MonoBehaviour
{
    [Header("Scene Info")]
    public string thisEntranceName;
    public string targetSceneName;
    public string targetSceneEntranceName;

    [Header("Zone Info")]    
    public LayerMask playerMask;
    public Transform entrance;
    public Transform exitWalkTarget;
    public Transform enterWalktarget;

    public CharacterDialogueProfile rejectDialogueProfile;
    public DetectiveObjective requiredObjective;

    private bool active;
    private bool triggered;

    [Space]
    public bool defaultEntrance;

    private void Start()
    {
        triggered = false;
    }

    public void SetZoneActive(bool isActive)
    {
        active = isActive;
    }

    private void Update()
    {
        if (!active) return;

        if (!triggered && IsPlayerColliding())
        {
            bool unlocked = (requiredObjective == null || ObjectiveManager.Instance.objectives[requiredObjective.key].IsComplete());

            if (unlocked)
            {
                SceneTransition();                
            }
            else
            {                
                RejectTransition();
            }

            triggered = true;
        }
    }

    private void RejectTransition()
    {        
        PlayerWalkToRejectPoint();        
        Player.Instance.Event_StoppedMoving.AddListener(DisplayRejectDialogue);        
    }

    private void DisplayRejectDialogue()
    {
        Debug.Log("Reject dialogue");
        InputManager.Instance.PauseGameplayInput(false);        
        UIDialogue.Instance.InitializeDialogue(rejectDialogueProfile, rejectDialogueProfile.GetCurrentDialogState(), null);
        Player.Instance.Event_StoppedMoving.RemoveListener(DisplayRejectDialogue);
        triggered = false;
    }

    private void SceneTransition()
    {
        PlayerWalkToPoint();
        SceneTransitionManager.Instance.SetSceneTransition(targetSceneName, targetSceneEntranceName);
    }

    private void PlayerWalkToPoint()
    {
        MovementFunction characterMovement = Player.Instance.GetMovementFunction();
        characterMovement.MoveToDestination(exitWalkTarget.position);
        InputManager.Instance.PauseGameplayInput(true);
    }

    private void PlayerWalkToRejectPoint()
    {
        MovementFunction characterMovement = Player.Instance.GetMovementFunction();
        characterMovement.MoveToDestination(enterWalktarget.position, true);
       // InputManager.Instance.PauseGameplayInput(true);
    }

    public bool IsPlayerColliding()
    {
        Collider[] colliders = Physics.OverlapBox(transform.position, transform.localScale / 2, transform.rotation, playerMask);

        foreach (Collider other in colliders)
        {
            Debug.Log(other.gameObject.name);

            if (other.gameObject.CompareTag("Player"))
            {
                return true;
            }
        }
        return false;
    }

    public virtual string SetDebugText()
    {
        string debugText = "";
        string sceneText = "Scene: " + targetSceneName + System.Environment.NewLine;
        string entranceText = "Entrance: " + targetSceneEntranceName;

        return debugText + sceneText + entranceText;
    }

#if UNITY_EDITOR
    protected virtual void OnDrawGizmos()
    {
        UnityEditor.Handles.Label(transform.position, SetDebugText());

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(exitWalkTarget.position, 0.5f);

        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(enterWalktarget.position, 0.5f);

        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(entrance.position, 0.5f);

        Gizmos.color = Color.red;
        Gizmos.matrix = transform.localToWorldMatrix;
        Vector3 pos = Vector3.zero;
        Gizmos.DrawWireCube(pos, Vector3.one);
    }
#endif
}


