using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DetectiveEvent
{
    public Evidence evidenceToGive;
    public Evidence evidenceToRemove;
    public DetectiveObjective objectiveToComplete;

    public void DoEvent()
    {
        if (objectiveToComplete)
        {
            string objectiveKey = objectiveToComplete.key;
            DetectiveObjective objective;

            if (ObjectiveManager.Instance.objectives.TryGetValue(objectiveKey, out objective))
            {
                ObjectiveManager.Instance.objectives[objectiveToComplete.key].SetComplete();
            } else
            {
                Debug.LogError(objectiveToComplete.name + " is not loaded.", objectiveToComplete);
            }
        }

        if (evidenceToGive)
        {
            string giveEvidenceKey = evidenceToGive.key;
            Evidence evidence;

            if (EvidenceManager.Instance.evidence.TryGetValue(giveEvidenceKey, out evidence))
            {
                EvidenceManager.Instance.gatheredEvidence.AddEvidence(EvidenceManager.Instance.evidence[evidenceToGive.key]);
            }
        }

        if (evidenceToRemove)
        {
            string evidenceKey = evidenceToRemove.key;
            Evidence removeEvidenceKey;

            //If evidence exists
            if (EvidenceManager.Instance.evidence.TryGetValue(evidenceKey, out removeEvidenceKey))
            {
                EvidenceManager.Instance.gatheredEvidence.RemoveEvidence(EvidenceManager.Instance.evidence[evidenceToRemove.key]);
            }
        }
    }
}
