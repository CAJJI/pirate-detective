using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

[DisallowMultipleComponent]
public abstract class MovementFunction : CharacterFunction
{
    public override CharacterFunctionType functionType => CharacterFunctionType.MOVEMENT;

    public ParticleSystem walkPFX;

    [System.Serializable]
    public class TargetDestination
    {
        public Vector3 position;
        public Interactable interactable;
        public Evidence evidence;
        public bool isMovingToInteract;
        public bool isMovingToPresent;
        public bool exists;
        public bool forced;


        public void SetPosition(Vector3 targetPosition, bool isForced = false)
        {
            position = targetPosition;
            interactable = null;
            isMovingToInteract = false;
            isMovingToPresent = false;
            forced = isForced;
            exists = true;
        }

        public void SetPosition(Interactable targetInteractable, bool isForced = false)
        {
            position = targetInteractable.transform.position;
            interactable = targetInteractable;
            isMovingToInteract = true;
            isMovingToPresent = false;
            forced = isForced;
            exists = true;
        }

        public void SetPosition(Interactable targetInteractable, Evidence targetEvidence, bool isForced = false)
        {
            position = targetInteractable.transform.position;
            interactable = targetInteractable;
            evidence = targetEvidence;
            isMovingToInteract = true;
            isMovingToPresent = true;
            forced = isForced;
            exists = true;
        }

        public void SetEmpty()
        {
            position = Vector3.zero;
            interactable = null;
            evidence = null;
            isMovingToInteract = false;
            isMovingToPresent = false;
            forced = false;
            exists = false;
        }
    }

    public TargetDestination targetDestination = new TargetDestination();

    public override void CF_Start()
    {
        character.agent.speed = character.properties.speed;
    }

    public override void CF_Update()
    {
        if (character.agent.pathPending) return;
        if (character.agent.pathStatus != NavMeshPathStatus.PathComplete) return;
        if (character.agent.isStopped) return;

        if (character.agent.remainingDistance <= character.agent.stoppingDistance)
        {
            if (targetDestination.isMovingToPresent)
            {
                targetDestination.interactable.PresentEvidenceTo(targetDestination.evidence);                                
            }

            else if (targetDestination.isMovingToInteract)
            {
                targetDestination.interactable.Interact();                            
            }

            StopAgent();
        }
    }

    protected virtual void StopAgent()
    {
        character.agent.isStopped = true;
        character.agent.ResetPath();
        //character.agent.velocity = Vector3.zero;
        targetDestination.SetEmpty();

        character.Event_StoppedMoving.Invoke();
    }

    public virtual void MoveToDestination(Vector3 targetPosition, bool forced = false)
    {
        targetDestination.SetPosition(targetPosition, forced);
        character.agent.SetDestination(targetDestination.position);
        character.agent.stoppingDistance = 0;

        character.Event_StartedMoving.Invoke();
    }

    public virtual void MoveToDestination(Interactable targetInteractable, bool forced = false)
    {
        targetDestination.SetPosition(targetInteractable, forced);
        character.agent.SetDestination(targetDestination.position);
        character.agent.stoppingDistance = character.properties.interactionDistance;

        character.Event_StartedMoving.Invoke();
    }

    public virtual void MoveToPresentToInteractable(Interactable targetInteractable, Evidence evidence)
    {

        StopAgent();

        targetDestination.SetPosition(targetInteractable, evidence);
        character.agent.SetDestination(targetDestination.position);
        character.agent.stoppingDistance = character.properties.interactionDistance;

        character.Event_StartedMoving.Invoke();
    }

    public void PlayWalkPFX()
    {
        if (walkPFX)
        {
            walkPFX.Play();
        }
    }


}
