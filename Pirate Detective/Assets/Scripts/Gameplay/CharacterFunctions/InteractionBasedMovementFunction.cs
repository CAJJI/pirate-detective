using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

[RequireComponent(typeof(Player))]
public class InteractionBasedMovementFunction : MovementFunction
{
    Player player { get { return character as Player; } }
    public LayerMask interactableLayer;

    private bool tracingPath;

    public override void CF_Awake()
    {

    }

    public override void CF_Start()
    {
        InputManager.Instance.GameplayInputEvent_PrimaryInteractDown.AddListener(Event_PrimaryInteractDown);
        InputManager.Instance.GameplayInputEvent_PrimaryInteractHoldDown.AddListener(Event_PrimaryInteractHoldDown);
        InputManager.Instance.GameplayInputEvent_PrimaryInteractHoldUp.AddListener(Event_PrimaryInteractHoldUp);

        UIHUDEvidenceBar.Instance.Event_OnEvidenceBarOpen.AddListener(StopAgent);
    }

    private void Event_PrimaryInteractDown()
    {
        if (CursorManager.Instance.IsMouseOverUI()) return;
        SetTargetDestinationFromRaycast();
    }
    private void Event_PrimaryInteractHoldDown()
    {
        if (CursorManager.Instance.IsMouseOverUI()) return;
        if (UIHUDEvidenceBar.Instance.IsOpen()) return;

        tracingPath = true;
    }
    private void Event_PrimaryInteractHoldUp()
    {
        if (CursorManager.Instance.IsMouseOverUI()) return;
        if (UIHUDEvidenceBar.Instance.IsOpen()) return;

        tracingPath = false;
    }

    public override void CF_Update()
    {
        base.CF_Update();

        if (!tracingPath) return;
        SetTargetDestinationFromRaycast();
    }

    private void SetTargetDestinationFromRaycast()
    {
        if (targetDestination.forced) return;

        player.Event_DestinationSet.Invoke();

        Ray ray = player.mainCamera.ScreenPointToRay(Input.mousePosition);

        if (!tracingPath)
        {
            RaycastHit[] hits;
            hits = Physics.RaycastAll(ray.origin, ray.direction, 1000, interactableLayer);

            if (hits.Length > 0)
            {
                Interactable targetInteractable = null;
                for (int i = 0; i < hits.Length; i++)
                {
                    Interactable interactable = hits[i].collider.GetComponent<Interactable>();
                    if (interactable)
                        targetInteractable = interactable;
                }

                if (targetInteractable)
                {
                    MoveToDestination(targetInteractable);
                    return;
                }

                MoveToDestination(hits[0].point);
                return;
            }
        }
    }

    protected override void StopAgent()
    {
        base.StopAgent();
        tracingPath = false;
    }
}
