using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public abstract class AnimatorFunction : CharacterFunction
{
    public override CharacterFunctionType functionType => CharacterFunctionType.ANIMATOR;

    public Vector3 facingDirection;
    public override void CF_Update()
    {
        SetAnimationSpeed();
        SetDirection();
    }

    protected virtual void SetDirection()
    {
        Vector3 cameraRelativeVelocity = Camera.main.transform.InverseTransformVector(facingDirection);
        cameraRelativeVelocity.y = 0;

        cameraRelativeVelocity = cameraRelativeVelocity.normalized;

        int direction = (int)Mathf.Sign(cameraRelativeVelocity.x);
        character.animator.SetInteger("Left/Right", direction);
    }

    protected virtual void SetAnimationSpeed()
    {
        float speedPercentage = 1;
        speedPercentage = Mathf.Clamp(speedPercentage, 0, 1);

        character.animator.SetFloat("AnimationSpeed", speedPercentage);
    }

}
