using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterFunction : MonoBehaviour
{
    public virtual CharacterFunctionType functionType => CharacterFunctionType.NONE;
    public Character character { get; set; }
    public virtual void CF_Awake() { }
    public virtual void CF_Start() { }
    public virtual void CF_Update() { }
    public virtual void CF_FixedUpdate() { }
}