using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(MovementFunction))]
public class LocomotionAnimatorFunction : AnimatorFunction
{
    private bool moving;

    public override void CF_Awake()
    {
        base.CF_Awake();

        character.Event_StartedMoving.AddListener(AnimationEvent_OnAgentMoveStart);
        character.Event_StoppedMoving.AddListener(AnimationEvent_OnAgentMoveEnd);
    }

    public override void CF_Start()
    {
        base.CF_Start();
    }

    public override void CF_Update()
    {
        SetAnimationSpeed();
        SetDirection();
    }

    protected override void SetDirection()
    {
        if (!moving) return;

        facingDirection = character.agent.velocity.normalized;

        Vector3 cameraRelativeVelocity = Camera.main.transform.InverseTransformVector(facingDirection);
        cameraRelativeVelocity.y = 0;
        cameraRelativeVelocity = Vector3.ClampMagnitude(cameraRelativeVelocity,1);

        float direction = cameraRelativeVelocity.x;
        character.animator.SetFloat("LeftRight", direction);
    }

    protected override void SetAnimationSpeed()
    {
        float agentVelocityMagnitude = character.agent.velocity.magnitude;

        float speedPercentage = agentVelocityMagnitude / character.properties.speed;
        speedPercentage = Mathf.Clamp(speedPercentage, 0, 1);

        character.animator.SetFloat("AnimationSpeed", speedPercentage);
    }

    private void AnimationEvent_OnAgentMoveStart()
    {
        moving = true;
        character.animator.SetTrigger("StartMoving");
    }

    private void AnimationEvent_OnAgentMoveEnd()
    {
        moving = false;
        character.animator.SetTrigger("StopMoving");
    }


}
