using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[DisallowMultipleComponent]
public class Character : Interactable
{
    //NavmeshAgent Events
    [HideInInspector] public UnityEvent Event_StartedMoving = new UnityEvent();
    [HideInInspector] public UnityEvent Event_StoppedMoving = new UnityEvent();
    [HideInInspector] public UnityEvent Event_DestinationSet = new UnityEvent();

    [Header("Character")]
    public NavMeshAgent agent;
    public Animator animator;
    public CharacterProperties properties;

    protected List<CharacterFunction> functions = new List<CharacterFunction>();
    public Dictionary<CharacterFunctionType, CharacterFunction> functionsAccessor = new Dictionary<CharacterFunctionType, CharacterFunction>();

    protected virtual void Awake()
    {
        functions = new List<CharacterFunction>(GetComponents<CharacterFunction>());

        AwakeAllCharacterMechanics();
    }

    protected virtual void Start()
    {
        StartAllCharacterFunctions();
    }

    protected virtual void Update()
    {
        UpdateAllCharacterFunctions();
    }

    protected virtual void FixedUpdate()
    {
        FixedUpdateAllCharacterFunctions();
    }

    protected virtual void AwakeAllCharacterMechanics()
    {
        foreach (CharacterFunction cf in functions)
        {
            functionsAccessor.Add(cf.functionType, cf);
            cf.character = this;
            cf.CF_Awake();
        }
    }
    protected virtual void StartAllCharacterFunctions()
    {
        foreach (CharacterFunction cf in functions)
        {
            cf.CF_Start();
        }
    }
    protected virtual void UpdateAllCharacterFunctions()
    {
        foreach (CharacterFunction cf in functions)
        {
            cf.CF_Update();
        }
    }

    protected virtual void FixedUpdateAllCharacterFunctions()
    {
        foreach (CharacterFunction cf in functions)
        {
            cf.CF_FixedUpdate();
        }
    }

    public MovementFunction GetMovementFunction()
    {
        return functionsAccessor[CharacterFunctionType.MOVEMENT] as MovementFunction;
    }

    public override void Interact()
    {
        base.Interact();
        Debug.Log(gameObject.name + " has been interacted with", gameObject);
    }



}
