using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{

    [Header("Dialogue")]
    public CharacterDialogueProfile dialogueProfile;

    public bool canPresentEvidenceTo = true;
    public virtual void Interact()
    {
        if (dialogueProfile)
        {
            UIDialogue.Instance.InitializeDialogue(dialogueProfile, dialogueProfile.GetCurrentDialogState(), this);
        }
    }
    public virtual void PresentEvidenceTo(Evidence evidence)
    {
        if (dialogueProfile && dialogueProfile.GetCurrentDialogState().evidenceToProgress == evidence)
        {
            Engine.Instance.objectiveManager.objectives[dialogueProfile.GetCurrentDialogState().completeOnEvidence.key].SetComplete();

            if (dialogueProfile)
            {
                UIDialogue.Instance.InitializeDialogue(dialogueProfile, dialogueProfile.GetCurrentDialogState(), this);
            }

            AudioManager.PlaySfx(AudioDef.Instance.evidencePresented);
            Debug.Log("Presented " + evidence.evidenceName + " to " + gameObject.name + ".\nSuccess");
        } else
        {            
            if (dialogueProfile)
            {
                CharacterDialogueProfile.DialogueState state = dialogueProfile.GetEvidenceDialogState(evidence);
                if (state != null)
                {
                    UIDialogue.Instance.InitializeDialogue(dialogueProfile, state, this);
                }
                else
                {
                    UIDialogue.Instance.InitializeDialogue(dialogueProfile, dialogueProfile.irrelevantEvidenceState, this);
                }
            }

            Debug.Log("Presented " + evidence.evidenceName + " to " + gameObject.name + ".\nFail");
        }
    }
}
