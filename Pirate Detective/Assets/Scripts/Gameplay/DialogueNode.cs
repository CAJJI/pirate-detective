using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Emotion
{
    Neutral,
    Happy,
    Scared,
    Sad,
    Excited,
    Intrigued
}

[System.Serializable]
public class DialogueNode
{
    [Header ("Dialogue")]
    public bool isPlayer;
    public Emotion emotion;
    [TextArea]
    public string dialogue;

    [Header("Animations")]
    public string speakerAnimation = "Talk";
    public string listenerAnimation = "Default";

    [Header("Audio")]
    public bool stopMusic;
    public Music switchTracksTo;

    [Header("Events")]
    public List<DetectiveEvent> eventsToDo = new List<DetectiveEvent>();
}