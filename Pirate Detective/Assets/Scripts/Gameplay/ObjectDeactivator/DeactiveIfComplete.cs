using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactiveIfComplete : ObjectDeactivator
{
    public override void OnSceneStart()
    {
        if (!Engine.Instance.objectiveManager.objectives[objective.key].IsComplete())
        {
            Engine.Instance.objectiveManager.objectives[objective.key].Event_OnComplete.AddListener(Event_DeavtivateObjects);
            return;
        }

        SetAllActive(false);

    }

    public void Event_DeavtivateObjects()
    {
        SetAllActive(false);
    }
}
