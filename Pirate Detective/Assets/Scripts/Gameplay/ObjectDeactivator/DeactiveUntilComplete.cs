using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactiveUntilComplete : ObjectDeactivator
{

    public override void OnSceneStart()
    {
        if (Engine.Instance.objectiveManager.objectives[objective.key].IsComplete())
        {
            SetAllActive(true);
            return;
        }

        Engine.Instance.objectiveManager.objectives[objective.key].Event_OnComplete.AddListener(Event_DeavtivateObjects);
    }

    public void Event_DeavtivateObjects()
    {
        SetAllActive(true);
    }

}
