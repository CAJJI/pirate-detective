using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDeactivator : MonoBehaviour
{

    public DetectiveObjective objective;

    public List<GameObject> objectsToDeactivate;

    public virtual void OnSceneStart()
    {
    }

    public void SetAllActive(bool isActive)
    {
        foreach (GameObject item in objectsToDeactivate)
        {
            item.SetActive(isActive);
        }
    }

}
