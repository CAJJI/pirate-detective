using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class Player : Character
{
    public static Player Instance { get; private set; }

    [Header("Player")]
    public Camera mainCamera;

    protected override void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        mainCamera = Camera.main;

        base.Awake();
    }




}
