using UnityEngine;

[ExecuteInEditMode]
public class BillboardSprite : MonoBehaviour
{
    private void Update()
    {
        Camera camera = Camera.main;
        Quaternion rotation = camera.transform.rotation;
        transform.LookAt(transform.position + rotation * Vector3.forward, rotation * Vector3.up);
    }
}
