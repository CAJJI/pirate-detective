using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EvidenceItemUI : MonoBehaviour, IPointerDownHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IDropHandler
{
    [HideInInspector]
    public Canvas canvasRef;
    [HideInInspector]
    public Evidence evidenceData;
    [HideInInspector]
    public RectTransform draggableChild;

    Image draggableImage;
    Image imageRef;
    Vector3 startingPosition;

    private void Start()
    {
        imageRef = GetComponent<Image>();
        imageRef.sprite = evidenceData.evidenceImage;
        draggableImage = draggableChild.GetComponent<Image>();
        draggableImage.enabled = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        imageRef.color = new Color(1, 1, 1, 0.5f);
        draggableImage.enabled = true;
        draggableImage.sprite = imageRef.sprite;
        draggableChild.position = (transform as RectTransform).position;
        //startingPosition = draggableChild.anchoredPosition;
    }

    public void OnDrag(PointerEventData eventData)
    {
        draggableChild.anchoredPosition += eventData.delta / canvasRef.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        imageRef.color = Color.white;
        draggableChild.anchoredPosition = startingPosition;
        draggableImage.enabled = false;
        //were gonna have to raycast here cause OnDrop dont work for non-ui
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) 
        {
            //send whatever message that they dragged evidence.
            Debug.Log("Evidence was dragged");
        }

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //throw new System.NotImplementedException();
    }

    public void OnDrop(PointerEventData eventData)
    {
        //canvasGroupRef.blocksRaycasts = true;
    }
}
