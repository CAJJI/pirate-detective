using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class UIButton : UIElement, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [Header("Button References")]
    [SerializeField]
    public bool isInteractable; //If the button is present, but the conditions for it are not met

    [SerializeField]
    public bool isClickable = true; //set to false if the button should be controlled by other system

    [Header("Button Events")]
    public UnityEvent onDown;

    public UnityEvent onActivate;
    public UnityEvent onDeactivate;

    protected virtual void Awake()
    {
        if (rectTransform != null) rectTransform = GetComponent<RectTransform>();
        if (canvasGroup != null) canvasGroup = GetComponent<CanvasGroup>();

        onDown.AddListener(ButtonEvent_OnButtonDown);
        onEnter.AddListener(ButtonEvent_OnButtonEnter);
        onExit.AddListener(ButtonEvent_OnButtonExit);
    }

    public virtual void AddButtonDownEvent(UnityAction cb)
    {
        onDown.AddListener(cb);
    }
    public virtual void AddButtonEnterEvent(UnityAction cb)
    {
        onEnter.AddListener(cb);
    }
    public virtual void AddButtonExitEvent(UnityAction cb)
    {
        onExit.AddListener(cb);
    }

    protected virtual void OnButtonDeactivate()
    {
        onDeactivate.Invoke();
    }
    protected virtual void OnButtonActivate()
    {
        onActivate.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!isClickable) return;
        if (!isInteractable) return;

        if (eventData.button == PointerEventData.InputButton.Left)
        {
            onDown.Invoke();
        }
    }
    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (!isClickable) return;
        if (!isInteractable) return;

        onEnter.Invoke();
    }
    public override void OnPointerExit(PointerEventData eventData)
    {
        if (!isClickable) return;
        if (!isInteractable) return;

        onExit.Invoke();
    }

    public virtual void OnButtonDown()
    {
        if (!isInteractable) return;

        onDown.Invoke();
    }
    public virtual void OnButtonEnter()
    {
        if (!isInteractable) return;

        onEnter.Invoke();
    }
    public virtual void OnButtonExit()
    {
        if (!isInteractable) return;

        onExit.Invoke();
    }

    public virtual void ButtonEvent_OnButtonDown()
    {

    }

    public virtual void ButtonEvent_OnButtonEnter()
    {

    }

    public virtual void ButtonEvent_OnButtonExit()
    {

    }

    public void SetInteractable(bool interactable)
    {
        if (interactable)
        {
            OnButtonActivate();
        }
        else
        {
            OnButtonDeactivate();
        }
        // SetSelectable(interactable);
        isInteractable = interactable;
    }

}

