using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public abstract class UIElement : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Element References")]
    public RectTransform rectTransform;
    public CanvasGroup canvasGroup;
    public CanvasGroup bodyCG;

    [Header("Element Events")]
    public UnityEvent onEnter;
    public UnityEvent onExit;

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        onEnter.Invoke();
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        onExit.Invoke();
    }
}
