using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EvidenceMenu : Singleton<EvidenceMenu>
{
    public Evidence testEvidence;

    public RectTransform draggableEvidenceObject;
    public RectTransform evidenceScrollPanel;
    public GameObject EvidenceUIPrefab;
    public float evidenceScrollTime = 1;

    int evidencePosition = 0;
    bool MenuIsOpen = false;
    bool evidenceLerpActive = false;
    Canvas UICanvas;
    Animator anim; 

    void Start()
    {
        UICanvas = GetComponentInParent<Canvas>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            AddEvidence(testEvidence);
        }
    }

    public void AddEvidence(Evidence evidenceToAdd) 
    {
        EvidenceItemUI evidenceUI = Instantiate(EvidenceUIPrefab, evidenceScrollPanel).GetComponent<EvidenceItemUI>();
        evidenceUI.canvasRef = UICanvas;
        evidenceUI.evidenceData = evidenceToAdd;
        evidenceUI.draggableChild = draggableEvidenceObject;
    }

    public void MoveRight() 
    {
        if (evidenceScrollPanel.childCount > 5 * (evidencePosition + 1) ) 
        {
            if (evidenceLerpActive == false) 
            {
                evidencePosition++;
                StartCoroutine(ScrollToPosition(evidencePosition * -500));   
            }
        }
    }

    public void MoveLeft() 
    {
        if (evidencePosition > 0)
        {
            if (evidenceLerpActive == false)
            {
                evidencePosition--;
                StartCoroutine(ScrollToPosition(evidencePosition * -500));
            }
        }
    }

    IEnumerator ScrollToPosition(int pos)
    {
        evidenceLerpActive = true;

        Vector2 startingPos = evidenceScrollPanel.anchoredPosition;
        float lerpTime = 0;
        while (evidenceScrollPanel.anchoredPosition.x != pos)
        {
            yield return null;
            lerpTime += Time.deltaTime;
            evidenceScrollPanel.anchoredPosition = Vector2.Lerp(startingPos, new Vector3(pos, startingPos.y), lerpTime / evidenceScrollTime);
        }

        evidenceLerpActive = false;
    }

    public void ToggleMenu() 
    {
        if (MenuIsOpen == false)
        {
            anim.Play("Open");
            MenuIsOpen = true;
        }
        else 
        {
            anim.Play("Close");
            MenuIsOpen = false;
        }
    }

}
