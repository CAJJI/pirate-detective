using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InspectMenu : MonoBehaviour, IDropHandler
{
    public Camera inspectionCamera;
    public Vector3 inspectionCameraOffset;

    float defaultCameraRectY;
    private void Start()
    {
        defaultCameraRectY = inspectionCamera.rect.y;
    }

    public void OnDrop(PointerEventData eventData)
    {
        EvidenceItemUI evidenceUI = eventData.pointerDrag.GetComponent<EvidenceItemUI>();
        if (evidenceUI != null)
        {
            //need to open inspection menu first
            if (inspectIsOpen == true)
                return;

            if(coroutineRunning == false)
                StartCoroutine(OpenInspectMenu(true));

            //we'll need to instantiate the evidence x distance in front of camera
            GameObject inspectionObject = Instantiate(evidenceUI.evidenceData.evidenceObject, inspectionCamera.transform);
            inspectionObject.transform.localPosition = inspectionCameraOffset;
            //if (evidenceUI.evidenceData.setStartingRotation == true) 
            //{
                inspectionObject.transform.eulerAngles = evidenceUI.evidenceData.startingRotation;
            //}

            Debug.Log(evidenceUI.evidenceData.evidenceName);
        }
    }

    Ray ray;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && inspectIsOpen != false)
        {
            ray = inspectionCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log("clicked on evidence!");
                InvestigatableEvidence investigatedEvidence = hit.collider.GetComponent<InvestigatableEvidence>();

                if (investigatedEvidence != null)
                {
                    //you clicked on the hidden evidence, add it to evidence inventory
                    EvidenceMenu.Instance.AddEvidence(investigatedEvidence.evidenceData);
                }

            }
        }

        Debug.DrawLine(ray.origin, ray.direction * 50, Color.red);

        if (Input.GetKeyDown(KeyCode.X)) 
        {
            StartCoroutine(OpenInspectMenu(false));
        }
    }

    bool inspectIsOpen = false;
    public float inspectorOpenTime = 1;
    bool coroutineRunning = false;
    IEnumerator OpenInspectMenu(bool open)
    {
        coroutineRunning = true;

        if (open == true)
        {
            inspectionCamera.enabled = true;
        }

        Vector2 startingValues = new Vector2(inspectionCamera.rect.height, inspectionCamera.rect.y);
        Vector2 targetCameraRectSize;
        if (open == true)
        {
            targetCameraRectSize.y = 0.133f; //y
            targetCameraRectSize.x = 0.75f; //height
        }
        else 
        {
            targetCameraRectSize = new Vector2(0.01f,0.01f);
        }

        float lerpTime = 0;
        while (inspectionCamera.rect.height != targetCameraRectSize.x) 
        {
            yield return null;
            lerpTime += Time.deltaTime;
            inspectionCamera.rect = new Rect(inspectionCamera.rect.x,
                                             inspectionCamera.rect.y,//Mathf.Lerp(startingValues.y, targetCameraRectSize.y, lerpTime / inspectorOpenTime),
                                             inspectionCamera.rect.width,
                                             Mathf.Lerp(startingValues.x, targetCameraRectSize.x, lerpTime / inspectorOpenTime));
        }

        inspectIsOpen = open;
        if (inspectIsOpen == false) 
        {
            inspectionCamera.enabled = false;
        }
        coroutineRunning = false;
    }


}
