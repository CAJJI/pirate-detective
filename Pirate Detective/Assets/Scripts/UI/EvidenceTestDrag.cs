using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EvidenceTestDrag : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        //this only works for UI, bleh
        EvidenceItemUI evidenceUI = eventData.pointerDrag.GetComponent<EvidenceItemUI>();
        if (evidenceUI != null) 
        {
            Debug.Log(evidenceUI.evidenceData.evidenceName);
        }
    }



}
