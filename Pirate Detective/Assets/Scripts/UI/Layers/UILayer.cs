using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UILayer : MonoBehaviour
{

    public virtual string UIKey { get { return ""; } }
    public virtual bool locked { get { return false; } }
    public bool isInFocus;
    public virtual void UI_Awake() { }

    public virtual void UI_Start() { }

    public virtual void UI_Update() { }

    public virtual void SetInput()
    {
        InputManager.Instance.SetControllerMap(ControllerMap.Menu);
    }

    public virtual void GainFocus()
    {
        gameObject.SetActive(true);
        isInFocus = true;
        SetInput();
    }

    public virtual void LoseFocus()
    {
        isInFocus = false;
        gameObject.SetActive(false);
    }

}
