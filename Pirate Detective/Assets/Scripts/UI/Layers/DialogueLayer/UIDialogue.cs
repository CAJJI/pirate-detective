using DG.Tweening;
using UnityEngine;
using TMPro;
public class UIDialogue : UILayer
{
    public static UIDialogue Instance { get; private set; }

    public override string UIKey { get { return "UIDialogue"; } }

    public Sfx startSfx;
    public Sfx continueSfx;
    public Sfx endSfx;

    public TMP_FontAsset defaultFont;
    public AnimationCurve cameraAnimCurve;

    public float cameraInfluence = 0.5f;
    public float cameraTransitionDuration = 0.3f;
    Vector3 defaultCameraPosition;
    bool initialized;

    public override void GainFocus()
    {
        base.GainFocus();
        Vector3 position = currentDialogInfo.participant ? currentDialogInfo.participant.transform.position : Player.Instance.transform.position;
        position = GetCameraPosition(position);

        Camera.main.transform.DOMove(position, cameraTransitionDuration).SetEase(cameraAnimCurve);
        Camera.main.orthographicSize = 14;
        Camera.main.DOOrthoSize(13, cameraTransitionDuration).SetEase(cameraAnimCurve);
    }

    public override void LoseFocus()
    {
        base.LoseFocus();
        if (initialized)
        {
            Camera.main.transform.DOMove(defaultCameraPosition, cameraTransitionDuration).SetEase(cameraAnimCurve);
        }
        Camera.main.orthographicSize = 13;
        Camera.main.DOOrthoSize(14, cameraTransitionDuration).SetEase(cameraAnimCurve);
    }

    public class CurrentDialogInfo
    {
        public CharacterDialogueProfile currentSpeaker;
        public CharacterDialogueProfile.DialogueState currentDialogueState;
        public Interactable participant;
        private int nodeIndex;
        public int NodeIndex { get { return nodeIndex; } }
        public bool exists;



        public void SetCurrentDialog(CharacterDialogueProfile characterProfile, CharacterDialogueProfile.DialogueState dialogState, Interactable targetParticipant)
        {
            currentSpeaker = characterProfile;
            currentDialogueState = dialogState;
            participant = targetParticipant;
            nodeIndex = 0;
            exists = true;
        }

        public void SetEmpty()
        {
            currentDialogueState = null;
            participant = null;
            nodeIndex = 0;
            exists = false;
        }

        public DialogueNode GetCurrentNode()
        {
            if (!exists) return null;

            return currentDialogueState.dialogueSegment.nodes[nodeIndex];
        }

        public void Iterate()
        {
            nodeIndex++;
        }

        public bool IsEndOfDialogue()
        {
            return (currentDialogueState.dialogueSegment.nodes.Count - 1 == nodeIndex);
        }
    }

    public CurrentDialogInfo currentDialogInfo = new CurrentDialogInfo();
    public UIDialogBox dialogBox;
    public UIItemGet itemGet;

    public override void UI_Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public override void UI_Start()
    {
        base.UI_Start();

        InputManager.Instance.MenuInputEvent_ConfirmDown.AddListener(Event_Confirm);

        defaultCameraPosition = Camera.main.transform.position;
        initialized = true;
    }

    public void InitializeDialogue(CharacterDialogueProfile characterProfile, CharacterDialogueProfile.DialogueState dialogueState, Interactable participant)
    {
        currentDialogInfo.SetCurrentDialog(characterProfile, dialogueState, participant);
        StartDialogue();
    }

    private void StartDialogue()
    {
        UIManager.Instance.SetFocus(this);

        SetupNode();
        HandleBGM();
        AudioManager.PlaySfx(startSfx);
    }

    private void SetupNode()
    {
        DisplayDialogBox();
        SetupEvidenceGetForNode();
    }

    private void DisplayDialogBox()
    {
        dialogBox.SetIsPlayer(currentDialogInfo.GetCurrentNode().isPlayer);
        dialogBox.SetActiveSpeaker(currentDialogInfo.currentSpeaker);

        if (currentDialogInfo.GetCurrentNode().isPlayer)
        {
            SetDialogueToPlayer(Player.Instance);
            AnimateCharacters(true);
            return;
        }

        SetDialogueToPlayer(currentDialogInfo.participant);
        AnimateCharacters(false);
    }

    private void AnimateCharacters(bool isPlayer)
    {
        string speakerAnimation = currentDialogInfo.GetCurrentNode().speakerAnimation;
        speakerAnimation = string.IsNullOrEmpty(speakerAnimation) ? "Talk" : speakerAnimation;

        string listenerAnimation = currentDialogInfo.GetCurrentNode().listenerAnimation;
        listenerAnimation = string.IsNullOrEmpty(listenerAnimation) ? "Default" : listenerAnimation;


        if (currentDialogInfo.participant && currentDialogInfo.participant is Character)
        {
            Character targetCharacter = currentDialogInfo.participant as Character;

            if (isPlayer)
            {
                Player.Instance.animator.Play(speakerAnimation, 1);
                targetCharacter.animator.Play(listenerAnimation, 1);
                return;
            }

            Player.Instance.animator.Play(listenerAnimation, 1);
            targetCharacter.animator.Play(speakerAnimation, 1);

            return;
        }

        if (isPlayer)
        {
            Player.Instance.animator.Play(speakerAnimation, 1);
            return;
        }
    }

    private void ResetAnimations()
    {

        Player.Instance.animator.Play("Default", 1);

        if (currentDialogInfo.participant && currentDialogInfo.participant is Character)
        {
            Character targetCharacter = currentDialogInfo.participant as Character;
            targetCharacter.animator.Play("Default", 1);
        }
    }

    public void SetDialogueToPlayer(Interactable targetInteractable)
    {
        Debug.Log(
            currentDialogInfo.currentDialogueState.dialogueSegment.name + ": "
            + targetInteractable.name + ": " + currentDialogInfo.GetCurrentNode().dialogue);

        dialogBox.SetText(currentDialogInfo.GetCurrentNode(), targetInteractable);
        AnimateDialogueBoxIn();
    }

    private int currentEvidenceToGetIndex;
    private int amountOfEvidenceToGet;
    Sequence itemGetTween;

    bool itemgetIntroAnim;
    bool itemgetOutroAnim;

    public void TryContinueDialogue()
    {

        if (dialogBox.isTypingText)
        {
            dialogBox.CompleteTypingText();
            return;
        }

        if (itemGet.GetActive())
        {
            if (itemgetIntroAnim)
            {
                itemGetTween.Kill();
                OnItemGetIntroComplete();
                return;
            }

            if (!itemgetIntroAnim && !itemgetOutroAnim)
            {
                PlayEvidenceGetOutro();
                return;
            }

            if (itemgetOutroAnim)
            {
                itemGetTween.Kill();
                OnItemGetOutroComplete();
                return;
            }

            return;
        }

        if (currentEvidenceToGetIndex < amountOfEvidenceToGet)
        {
            PlayEvidenceGetIntro();
            return;
        }


        if (currentDialogInfo.IsEndOfDialogue())
        {
            EndDialogue();
            return;
        }


        ContinueDialogue();
    }

    private void PlayEvidenceGetIntro()
    {
        itemGetTween = DOTween.Sequence();
        itemGetTween.AppendInterval(3);
        itemGetTween.Play().OnComplete(OnItemGetIntroComplete);
        itemgetIntroAnim = true;

        itemGet.gameObject.SetActive(true);
        itemGet.SetItemGetActive(true);
        itemGet.SetGraphic(currentDialogInfo.GetCurrentNode().eventsToDo[currentEvidenceToGetIndex].evidenceToGive);
        itemGet.animator.enabled = true;
        itemGet.animator.Play("Intro", 0);
    }

    private void PlayEvidenceGetOutro()
    {
        itemGetTween = DOTween.Sequence();
        itemGetTween.AppendInterval(0.45f);
        itemGetTween.Play().OnComplete(OnItemGetOutroComplete);
        itemgetOutroAnim = true;
        itemGet.animator.enabled = true;
        itemGet.animator.Play("Outro", 0);
    }

    public void OnItemGetIntroComplete()
    {
        itemgetIntroAnim = false;
        itemGet.animator.Play("Default", 0);
    }

    public void OnItemGetOutroComplete()
    {
        itemgetOutroAnim = false;
        itemGet.animator.Play("Default", 0);

        itemGet.Clear();
        itemGet.SetItemGetActive(false);
        itemGet.gameObject.SetActive(false);
        currentEvidenceToGetIndex++;
    }

    private void SetupEvidenceGetForNode()
    {
        currentEvidenceToGetIndex = 0;
        amountOfEvidenceToGet = 0;

        if (currentDialogInfo.GetCurrentNode().eventsToDo.Count > 0)
        {
            foreach (DetectiveEvent eventToDo in currentDialogInfo.GetCurrentNode().eventsToDo)
            {
                if (eventToDo.evidenceToGive)
                {
                    if (!EvidenceManager.Instance.gatheredEvidence.HasGathered(eventToDo.evidenceToGive))
                    {
                        amountOfEvidenceToGet++;
                        continue;
                    }
                }
            }
        }
    }

    public void ContinueDialogue()
    {
        TriggerEventsToDo();
        currentDialogInfo.Iterate();
        SetupNode();
        HandleBGM();

        AudioManager.PlaySfx(continueSfx);
    }

    private void HandleBGM()
    {

        if (currentDialogInfo.GetCurrentNode().switchTracksTo && currentDialogInfo.GetCurrentNode().switchTracksTo != AudioManager.Instance.GetCurrentMusicFile())
        {
            AudioManager.PlayMusic(currentDialogInfo.GetCurrentNode().switchTracksTo);
        }

        if (currentDialogInfo.GetCurrentNode().stopMusic && AudioManager.Instance.musicAudioSource.isPlaying)
        {
            AudioManager.Instance.musicAudioSource.Stop();
        }
        else if (!currentDialogInfo.GetCurrentNode().stopMusic && !AudioManager.Instance.musicAudioSource.isPlaying)
        {
            AudioManager.Instance.musicAudioSource.Play();
        }
    }

    public void EndDialogue()
    {
        TriggerEventsToDo();
        AnimateDialogueBoxOut(ExitUIDialogue);
        AudioManager.PlaySfx(endSfx);

    }

    public void TriggerEventsToDo()
    {
        DialogueNode node = currentDialogInfo.GetCurrentNode();

        if (node.eventsToDo.Count == 0) return;

        foreach (DetectiveEvent eventToDo in node.eventsToDo)
        {
            eventToDo.DoEvent();
        }
    }

    public void ExitUIDialogue()
    {
        if (!string.IsNullOrEmpty(currentDialogInfo.currentDialogueState.sceneTransition))
        {
            SceneTransitionManager.Instance.SetSceneTransition(currentDialogInfo.currentDialogueState.sceneTransition, "");
            return;
        }

        if (currentDialogInfo.currentDialogueState.resetToDefaultMusic)
        {
            AudioManager.PlayMusic(AudioDef.Instance.gameplayMusic);

            if (!AudioManager.Instance.musicAudioSource.isPlaying)
            {
                AudioManager.StopMusic();
            }
        }


        ResetAnimations();
        currentDialogInfo.SetEmpty();
        dialogBox.ClearText();

        if (!AudioManager.Instance.musicAudioSource.isPlaying)
        {
            AudioManager.Instance.musicAudioSource.Play();
        }

        UIManager.Instance.SetFocus("UIHUD");
    }

    private void AnimateDialogueBoxIn()
    {
        dialogBox.canvasGroup.alpha = 0;
        dialogBox.rectTransform.localScale = Vector3.zero;

        Sequence dialogBoxAppearsTween = DOTween.Sequence();
        dialogBoxAppearsTween.Append(dialogBox.canvasGroup.DOFade(1, 0.1f));
        dialogBoxAppearsTween.Append(dialogBox.rectTransform.DOScale(Vector3.one, 0.1f));

        dialogBoxAppearsTween.Play();
    }

    private void AnimateDialogueBoxOut(TweenCallback callback)
    {
        dialogBox.canvasGroup.alpha = 1;
        dialogBox.rectTransform.localScale = Vector3.one;

        Sequence dialogBoxDisppearsTween = DOTween.Sequence();
        dialogBoxDisppearsTween.Append(dialogBox.canvasGroup.DOFade(0, 0.1f));
        dialogBoxDisppearsTween.Append(dialogBox.rectTransform.DOScale(Vector3.zero, 0.1f));

        dialogBoxDisppearsTween.Play().OnComplete(callback);
    }

    public void Event_Confirm()
    {
        if (!isInFocus) return;
        //  if (Engine.Instance.cursorManager.IsMouseOverUI()) return;

        TryContinueDialogue();
    }

    public Vector3 GetCameraPosition(Vector3 source)
    {
        Vector3 forward = Camera.main.transform.rotation * Vector3.forward;
        source -= forward * 50;
        Vector3 direction = source - defaultCameraPosition;
        return defaultCameraPosition + (direction * cameraInfluence);
    }
}
