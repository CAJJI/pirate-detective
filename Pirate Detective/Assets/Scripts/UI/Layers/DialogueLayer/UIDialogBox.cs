using TMPro;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDialogBox : MonoBehaviour
{
    public bool hidden;

    public float charactersPerSecond = 10;
    public float timePerCharacter { get { return 1 / charactersPerSecond; } }
    public float punctuationDelay;

    public bool pauseOnPunctuations = true;
    string dialogue;
    public bool isTypingText;
    float currentTypingDelay;
    float timeSinceLastCharacter;

    int currentCharacterIndex = 0;

    public TextMeshProUGUI dialogueText;
    public Image icon;
    public Image backingOuter;
    public Image backingInner;

    Action onComplete;

    public float offset = 3;

    public RectTransform rectTransform;
    public CanvasGroup canvasGroup;
    public Interactable target;

    List<char> delayedCharacters = new List<char>()
    {
        '.', '!', '?', '~', '-',
    };

    List<char> ignoredCharacters = new List<char>()
    {
        ' '
    };

    bool previouslyPunctuated;

    public CharacterDialogueProfile activeSpeaker;
    bool isPlayer;

    private void Update()
    {
        //UpdateText();
        RevealText();
        FollowTarget();
    }

    private void FollowTarget()
    {
        if (!target) return;
        rectTransform.position = Camera.main.WorldToScreenPoint(target.transform.position + Vector3.up * offset);
    }

    void RevealText()
    {
        if (!isTypingText) return;

        timeSinceLastCharacter += Time.deltaTime;

        if (previouslyPunctuated)
        {
            if (timeSinceLastCharacter >= timePerCharacter + punctuationDelay)
            {
                timeSinceLastCharacter = 0;
                currentCharacterIndex++;
                previouslyPunctuated = false;
            }
        }
        else
        {
            if (timeSinceLastCharacter >= timePerCharacter)
            {
                timeSinceLastCharacter = 0;
                currentCharacterIndex++;
            }
        }

        char currentCharacter = dialogueText.text[currentCharacterIndex];

        if (ignoredCharacters.Contains(currentCharacter))
        {
            currentCharacterIndex++;
        }

        if (pauseOnPunctuations)
        {
            if (delayedCharacters.Contains(currentCharacter))
                previouslyPunctuated = true;
        }

        int charactersShownLength = currentCharacterIndex + 1;

        if (dialogueText.maxVisibleCharacters != charactersShownLength)
        {
            string currentText = dialogueText.text.Substring(0, dialogueText.maxVisibleCharacters);
            PlaySfx(currentText, dialogueText.text);
        }

        dialogueText.maxVisibleCharacters = charactersShownLength;

        if (charactersShownLength == dialogueText.text.Length)
        {
            isTypingText = false;
            timeSinceLastCharacter = 0;
            currentCharacterIndex = 0;
        }

    }

    void UpdateText()
    {
        if (isTypingText)
        {
            //if (GameManager.isPaused) return;

            if (currentTypingDelay > 0)
            {
                currentTypingDelay -= Time.deltaTime;
                return;
            }

            int addCharacterCount = Mathf.FloorToInt(Time.deltaTime * charactersPerSecond);

            if (addCharacterCount <= 0)
            {
                timeSinceLastCharacter += Time.deltaTime;
                addCharacterCount = Mathf.FloorToInt(timeSinceLastCharacter * charactersPerSecond);
                if (addCharacterCount <= 0)
                    return;
            }

            timeSinceLastCharacter = 0;

            string currentText = dialogueText.text;

            if (currentText.Length + addCharacterCount > dialogue.Length)
                addCharacterCount = dialogue.Length - currentText.Length;

            string difference = dialogue.Substring(currentText.Length, addCharacterCount);

            if (pauseOnPunctuations)
            {
                char punctuation = 'x';
                int activeIndex = 0;
                for (int i = 0; i < delayedCharacters.Count; i++)
                {
                    int index = difference.IndexOf(delayedCharacters[i]);
                    if (index >= 0 && index <= activeIndex)
                    {
                        punctuation = delayedCharacters[i];
                    }
                }
                if (punctuation != 'x')
                {
                    difference = difference.Substring(0, difference.IndexOf(punctuation) + 1);
                    if (previouslyPunctuated && delayedCharacters.Contains(difference[0]))
                        currentTypingDelay = 0.01f;
                    else
                        currentTypingDelay = 0.2f;

                    previouslyPunctuated = true;
                }
                else
                {
                    previouslyPunctuated = false;
                }
            }
            else
            {
                previouslyPunctuated = false;
            }

            dialogueText.text += difference;

            PlaySfx(currentText, dialogue);

            if (dialogueText.text == dialogue)
            {
                isTypingText = false;
            }
        }
    }

    public void OnNodeComplete()
    {
       // TriggerEventsToDo();

    }

    public void CompleteTypingText()
    {
        isTypingText = false;
        dialogueText.maxVisibleCharacters = int.MaxValue;
    }

    public void OnEnd()
    {
        onComplete?.Invoke();
        onComplete = null;
    }

    public void ClearText()
    {
        target = null;

        dialogue = "";
        dialogueText.text = "";
        isTypingText = false;
    }

    public void SetStyle(CharacterDialogueProfile profile)
    {
        if (!profile.font)
        {
            dialogueText.font = UIDialogue.Instance.defaultFont;
        } else
        {
            dialogueText.font = profile.font;
        }

        if (profile.icon)
        {
            icon.gameObject.SetActive(true);
            icon.sprite = profile.icon;
        } else
        {
            icon.gameObject.SetActive(false);
            icon.sprite = null;
        }

        backingInner.color = profile.dialogueBoxInnerColor;
        backingOuter.color = profile.dialogueBoxOuterColor;
        dialogueText.color = profile.fontColor;
    }

    public void SetText(DialogueNode node, Interactable speaker)
    {
        SetStyle(speaker.dialogueProfile);

        target = speaker;

        dialogueText.text = node.dialogue.Trim();

        currentCharacterIndex = 0;
        dialogueText.maxVisibleCharacters = 0;
        isTypingText = true;
    }

    public void SetIsPlayer(bool state)
    {
        isPlayer = state;
    }

    public void SetActiveSpeaker(CharacterDialogueProfile speaker)
    {
        activeSpeaker = speaker;
    }

    void PlaySfx(string currentText, string fullText)
    {
        string remainingText = fullText.Remove(0, currentText.Length);

        if (remainingText.Length == 0 || delayedCharacters.Contains(remainingText[0]) || remainingText[0] == ' ')
            return;

        bool isExclamation = false;
        for (int i = 0; i < remainingText.Length; i++)
        {
            if (delayedCharacters.Contains(remainingText[i]))
            {
                if (remainingText[i] == '!')
                    isExclamation = true;
                break;
            }
        }

        int vocalExclamationIncrease = AudioDef.Instance.playerVocalExclamationIncrease;

        SpeechNote note = AudioDef.Instance.playerRootSpeechNote;
        if (activeSpeaker != null && !isPlayer)
        {
            note = activeSpeaker.rootSpeechNote;
            vocalExclamationIncrease = activeSpeaker.vocalExclamationIncrease;
        }
        int value = remainingText[0];
        value %= AudioDef.Instance.vocalRange;
        note += value;

        if (isExclamation)
            note += vocalExclamationIncrease;

        AudioManager.PlaySpeech(note);
    }
}
