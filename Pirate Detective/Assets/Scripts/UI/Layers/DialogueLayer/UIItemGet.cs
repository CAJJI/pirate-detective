using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class UIItemGet : MonoBehaviour
{
    public Animator animator;

    public Image graphic;

    public TextMeshProUGUI getMessage;
    public TextMeshProUGUI getMessageEffect;
    public TextMeshProUGUI evidenceName;
    public TextMeshProUGUI description;

    private bool active;

    public void SetGraphic(Evidence evidence)
    {
        graphic.sprite = evidence.evidenceImage;

        getMessage.text = evidence.testimony ? "New Testimony!" : "New Evidence";
        getMessageEffect.text = evidence.testimony ? "New Testimony!" : "New Evidence";
        evidenceName.text = evidence.evidenceName;
        description.text = evidence.evidenceDescription;
    }

    public void Clear()
    {
        graphic.sprite = null;

        getMessage.text = "";
        getMessageEffect.text = "";
        evidenceName.text = "";
        description.text = "";
    }

    public void SetItemGetActive(bool isActive)
    {
        active = isActive;
    }  

    public bool GetActive()
    {
        return active;
    }
}
