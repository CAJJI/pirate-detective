using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILogEntry : UIEntry
{
    [HideInInspector] public LogEntry entry;

    public RectTransform rectTransform;
    public GameObject visiblePanel;
    public GameObject unknownPanel;
    public GameObject hiddenPanel;


    public void SetLog(LogEntry targetEntry)
    {
        entry = targetEntry;
    }

    public void SetVisible()
    {
        visiblePanel.SetActive(true);
        unknownPanel.SetActive(false);
        hiddenPanel.SetActive(false);
    }

    public void SetUnknown()
    {
        visiblePanel.SetActive(false);
        unknownPanel.SetActive(true);
        hiddenPanel.SetActive(false);
    }

    public void SetHidden()
    {
        visiblePanel.SetActive(false);
        unknownPanel.SetActive(false);
        hiddenPanel.SetActive(true);
    }

    public void SetInvisible()
    {
        visiblePanel.SetActive(false);
        unknownPanel.SetActive(false);
        hiddenPanel.SetActive(false);
    }
}
