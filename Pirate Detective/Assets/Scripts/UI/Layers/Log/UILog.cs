using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UILog : UILayer
{
    public static UILog Instance { get; private set; }
    public override string UIKey { get { return "UILog"; } }

    [Header("Settings")]
    public LogData data;
    private int pageInterval = 3;
    public AnimationCurve introCurveTranslate;
    public AnimationCurve introCurveRotate;

    [Header("Transforms")]
    public RectTransform mainPanel;
    public CanvasGroup overlay;
    public RectTransform slider;
    public RectTransform timePanel;
    public RectTransform charactersHeader;

    [Header("Prefabs")]
    public UILogEntry logEntry;
    public UILogEntry nullLogEntry;
    public UIEntry timeEntry;
    public UICharacterEntry characterEntry;
    public RectTransform logPanel;

    private Dictionary<string, RectTransform> logPanels = new Dictionary<string, RectTransform>();
    private Dictionary<LogData.LogCharacter, UICharacterEntry> characters = new Dictionary<LogData.LogCharacter, UICharacterEntry>();
    private List<UILogEntry> entriesToCheck = new List<UILogEntry>();
    private Dictionary<UILogEntry, LogData.LogCharacter> logEntrysCharacter = new Dictionary<UILogEntry, LogData.LogCharacter>();
    private Dictionary<LogEntry, UILogEntry> logEntrysLog = new Dictionary<LogEntry, UILogEntry>();

    private int amountOfPages = 0;
    private float pageSize = 0;
    private int currentPageIndex;

    private bool isAnimating;

    public override void UI_Awake()
    {
        base.UI_Awake();

        if (Instance == null)
        {
            Instance = this;
        }

        Setup();
    }

    public override void UI_Start()
    {
        // base.UI_Start();
    }

    public override void UI_Update()
    {
        //  base.UI_Update();
    }

    public override void GainFocus()
    {
        base.GainFocus();
    }

    public override void LoseFocus()
    {
        base.LoseFocus();
    }

    public void ButtonEvent_OpenLog()
    {
        UIManager.Instance.SetFocus(this);
        EvaluateEntries();

        // Debug.Break();
        AnimateIn();

    }

    private void AnimateIn()
    {
        mainPanel.anchoredPosition += Vector2.down * 500;
        mainPanel.eulerAngles = new Vector3(0, 0, -20);
        overlay.alpha = 0;

        Sequence openCloseTween = DOTween.Sequence();

        openCloseTween.Append(mainPanel.DOAnchorPosY(0, 0.4f)).SetEase(introCurveTranslate);
        openCloseTween.Join(mainPanel.DORotate(Vector3.zero, 0.4f)).SetEase(introCurveRotate);
        openCloseTween.Join(overlay.DOFade(1, 0.4f));

        openCloseTween.Play();
    }


    public void ButtonEvent_CloseLog()
    {
        AnimateOut(() => { UIManager.Instance.SetFocus("UIHUD"); });
    }

    private void AnimateOut(TweenCallback cb)
    {
        mainPanel.anchoredPosition = Vector2.zero;
        mainPanel.eulerAngles = new Vector3(0, 0, 0);
        overlay.alpha = 1;

        Sequence openCloseTween = DOTween.Sequence();

        openCloseTween.Append(mainPanel.DOAnchorPosY(-700, 0.4f)).SetEase(introCurveTranslate);
        openCloseTween.Join(mainPanel.DORotate(new Vector3(0, 0, 30), 0.4f).SetEase(introCurveRotate));
        openCloseTween.Join(overlay.DOFade(0, 0.4f));

        openCloseTween.Play().OnComplete(cb);
    }


    public void ButtonEvent_MoveRight()
    {
        if (isAnimating) return;
        isAnimating = true;


        float currentXPos = currentPageIndex * -(pageSize * pageInterval);
        slider.anchoredPosition = new Vector2(currentXPos, 0);

        Sequence pageTurnTween = DOTween.Sequence();

        if ((currentPageIndex + 1) + 1 > amountOfPages)
        {
            pageTurnTween.Append(slider.DOPunchAnchorPos(Vector2.right * 10, 0.3f));
            pageTurnTween.Play().OnComplete(OnFinishTween);
            return;
        }

        currentPageIndex++;

        float targetXPos = currentPageIndex * -(pageSize * pageInterval);

        pageTurnTween.Append(slider.DOAnchorPosX(targetXPos, 0.3f));
        pageTurnTween.Play().OnComplete(OnFinishTween);

    }

    public void ButtonEvent_MoveLeft()
    {
        if (isAnimating) return;
        isAnimating = true;

        float currentXPos = currentPageIndex * -(pageSize * pageInterval);
        slider.anchoredPosition = new Vector2(currentXPos, 0);

        Sequence pageTurnTween = DOTween.Sequence();

        if ((currentPageIndex) - 1 < 0)
        {
            pageTurnTween.Append(slider.DOPunchAnchorPos(Vector2.left * 10, 0.3f));
            pageTurnTween.Play().OnComplete(OnFinishTween);
            return;
        }

        currentPageIndex--;

        float targetXPos = currentPageIndex * -(pageSize * pageInterval);

        pageTurnTween.Append(slider.DOAnchorPosX(targetXPos, 0.3f));
        pageTurnTween.Play().OnComplete(OnFinishTween);
    }

    private void OnFinishTween()
    {
        isAnimating = false;
    }

    private void Setup()
    {
        foreach (LogData.LogCharacter character in data.characters)
        {
            //instantiate and store data for character headers
            UICharacterEntry characterEntryInstance = Instantiate(characterEntry, charactersHeader).GetComponent<UICharacterEntry>();
            characters.Add(character, characterEntryInstance);
            characterEntryInstance.Initialize(character);

            //instantiate and store data for log panels for each character
            RectTransform logPanelInstance = Instantiate(logPanel, slider).GetComponent<RectTransform>();
            logPanels.Add(character.name, logPanelInstance);

            //instantiate and store data for each characters individual logs in each panel
            foreach (LogEntry entry in character.logEntries)
            {
                UILogEntry logEntryInstance = Instantiate(logEntry, logPanelInstance).GetComponent<UILogEntry>();
                logEntryInstance.SetLog(entry);
                logEntrysLog.Add(entry, logEntryInstance);
                entriesToCheck.Add(logEntryInstance);
                logEntrysCharacter.Add(logEntryInstance, character);
            }
        }

        for (int i = 0; i < data.amountOfHours; i++)
        {
            UIEntry timeEntryInstance = Instantiate(timeEntry, timePanel).GetComponent<UIEntry>();

            //convert time form int to hhtt format
            int val = i + data.startTime;
            TimeSpan result = TimeSpan.FromHours(val);
            DateTime time = DateTime.Today.Add(result);
            string displayTime = time.ToString("hh tt");

            timeEntryInstance.SetText(displayTime);
        }

        float pageVal = data.amountOfHours / pageInterval;
        amountOfPages = Mathf.FloorToInt(pageVal);

        pageSize = logEntry.rectTransform.sizeDelta.x;

    }

    private void EvaluateEntries()
    {
        foreach (LogData.LogCharacter character in data.characters)
        {
            //character does not have reveal set
            if (!character.revealOnComplete)
            {
                Debug.LogError("No character reveal set for " + character.name);
                continue;
            }

            //character has been revealed
            if (character.revealOnComplete.IsComplete())
            {

                foreach (LogEntry entry in character.logEntries)
                {
                    UILogEntry currentLog = logEntrysLog[entry];

                    //if the log has no entry
                    if (!entry)
                    {
                        Debug.LogWarning("No entry set");
                        continue;
                    }

                    //if the log is null
                    if (entry.isNull)
                    {
                        //if not revealed as null, set unknown
                        if (entry.revealOnComplete.IsComplete())
                        {
                            currentLog.SetHidden();
                            continue;
                        }

                        //set as null
                        currentLog.SetUnknown();
                        continue;
                    }

                    //if hasn't been unlocked, set as unknown
                    if (!entry.revealOnComplete.IsComplete())
                    {
                        currentLog.SetUnknown();
                        continue;
                    }

                    //if no case above applies, log is visible
                    currentLog.SetLog(entry.GetCurrentLogEntry());
                    currentLog.SetText(currentLog.entry.description);
                    currentLog.SetVisible();
                    continue;
                }
            }

            //if character has not been unlocked, set all of their logs to invisible
            if (!character.revealOnComplete.IsComplete())
            {
                characters[character].SetUnknown();

                foreach (LogEntry entry in character.logEntries)
                {
                    UILogEntry currentLogEntry = logEntrysLog[entry];

                    currentLogEntry.SetInvisible();
                }
            }
        }
    }
}
