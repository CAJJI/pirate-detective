using TMPro;
using UnityEngine;

public class UIEntry : MonoBehaviour
{
    public TextMeshProUGUI text;
   
    public void SetText(string targetText)
    {
        text.text = targetText;
    }
}
