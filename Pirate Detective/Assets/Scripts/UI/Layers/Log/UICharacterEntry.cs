using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICharacterEntry : UIEntry
{

    public LogData.LogCharacter character;

    public GameObject visiblePanel;
    public Image icon;

    public void Initialize(LogData.LogCharacter targetCharacter)
    {
        character = targetCharacter;
        text.text = targetCharacter.name;
        icon.sprite = targetCharacter.icon;
    }

    public void SetKnown()
    {
        visiblePanel.SetActive(true);
    }

    public void SetUnknown()
    {
        visiblePanel.SetActive(false);
    }

}
