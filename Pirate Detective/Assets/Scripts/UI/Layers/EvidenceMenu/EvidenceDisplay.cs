using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EvidenceDisplay : MonoBehaviour
{
    public Image icon;
    public Image iconShadow;
    public bool newNotif;
    public GameObject notification;
    public RectTransform rectTransform;
    public UIButton button;
    public Evidence evidence;
    public int indexofButton;

    public void Initialize(Evidence targetEvidence, int indexToAssign)
    {
        icon.sprite = targetEvidence.evidenceImage;
        iconShadow.sprite = targetEvidence.evidenceImage;
        evidence = targetEvidence;
        indexofButton = indexToAssign;
    }

    public void SetImageVisible(bool visible)
    {
        icon.gameObject.SetActive(visible);
        iconShadow.gameObject.SetActive(visible);
    }

    public void SetNotification(bool visible)
    {
        notification.gameObject.SetActive(visible);
        newNotif = visible;
    }
}
