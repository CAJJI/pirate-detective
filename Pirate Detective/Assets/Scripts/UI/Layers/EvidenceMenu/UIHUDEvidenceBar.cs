using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class UIHUDEvidenceBar : UILayer
{
    public static UIHUDEvidenceBar Instance { get; private set; }

    [HideInInspector] public UnityEvent Event_OnEvidenceBarOpen = new UnityEvent();

    public EvidenceDisplay evidenceButton;
    public LayerMask interactableMask;

    public TextMeshProUGUI evidenceName;
    public TextMeshProUGUI evidenceDescription;
    public RectTransform evidenceInfoBubble;
    public RectTransform evidenceInfoBubbleTail;
    public ParentConstraint evidenceInfoConstraint;
    public ParentConstraint evidenceInfoTailConstraint;

    public int currentNumberOfEvidenceInInventory;
    public int numberOfEvidenceToDisplay;

    public List<EvidenceDisplay> evidenceInHotbar;

    public Interactable targetInteractable;

    public Sfx openSfx;
    public Sfx closeSfx;

    private bool overInspectionButton;

    public UIButton inspectButton;
    public RectTransform inspectButtonGraphic;

    [System.Serializable]
    public class DraggableIcon
    {

        public RectTransform rectTransform;
        public Image image;
        public bool isDragging;
        public EvidenceDisplay hotbarItem;

        public Evidence evidence;

        public void SetIcon(EvidenceDisplay evi)
        {
            hotbarItem = evi;
            evidence = evi.evidence;
            image.sprite = evi.evidence.evidenceImage;
            isDragging = true;

            rectTransform.gameObject.SetActive(true);
        }

        public void SetEmpty()
        {
            hotbarItem = null;

            evidence = null;
            image.sprite = null;
            isDragging = false;

            rectTransform.gameObject.SetActive(false);
        }

    }

    public DraggableIcon draggableIcon = new DraggableIcon();

    public AnimationCurve openCloseCurve;
    public AnimationCurve openCloseCurveFlat;

    public bool open = false;

    public RectTransform slider;
    public RectTransform evidencePanel;
    public RectTransform bar;

    private float evidencePanelSize;
    private float spacing;
    private Vector2 buttonSize;
    private float closePosX;
    private float openPosX;
    private int amountOfScreens;

    private bool isAnimating;

    private int screenIndex = 0;

    Sequence slideBarTween;
    Sequence moveScreensTween;
    Sequence popInfoTween;
    public override void UI_Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        base.UI_Awake();
    }

    public override void UI_Start()
    {
        base.UI_Start();
        Player.Instance.Event_DestinationSet.AddListener(Event_AutoClose);
        InputManager.Instance.GameplayInputEvent_PrimaryInteractUp.AddListener(Event_LetGoOfDraggableIcon);
        inspectButton.onEnter.AddListener(Event_OnInspectEnter);
        inspectButton.onExit.AddListener(Event_OnInspectExit);

    }

    //float refRotVelocity = 0.0f;
    //float rotAmount = 0;

    Vector3 followPosition;
    public float followAmount = 0.1f;
    public float followGravity = 10;

    public override void UI_Update()
    {
        base.UI_Update();

        if (!draggableIcon.isDragging)
        {
            followPosition = Input.mousePosition;
            return;
        }

        draggableIcon.rectTransform.position = Input.mousePosition;

        Vector3 gravity = Input.mousePosition + Vector3.down * (followGravity * Screen.height/1080);
        followPosition = Vector3.Lerp(followPosition, gravity, followAmount * Time.deltaTime);
        Vector3 direction = followPosition - Input.mousePosition;
        float angle = Vector3.Angle(Vector3.down, direction);
        if (followPosition.x < gravity.x)
            angle *= -1;

        draggableIcon.rectTransform.eulerAngles = new Vector3(0, 0, angle);

        //float dragRotTarget = InputManager.Instance.LookDirection.x * -1000 * Time.deltaTime;
        //rotAmount = Mathf.SmoothDamp(rotAmount, dragRotTarget, ref refRotVelocity, 0.2f);
        //draggableIcon.rectTransform.eulerAngles = new Vector3(0, 0, rotAmount);

        RaycastForInteractable();
    }



    private void RaycastForInteractable()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit[] hits;
        hits = Physics.RaycastAll(ray.origin, ray.direction, 1000, interactableMask);

        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                targetInteractable = hits[i].collider.GetComponent<Interactable>();

                if (targetInteractable != null && targetInteractable.canPresentEvidenceTo)
                {
                    return;
                }
            }
        }

        targetInteractable = null;
    }


    public void CalculateSizes()
    {
        buttonSize = evidenceButton.rectTransform.sizeDelta;
        spacing = bar.GetComponent<HorizontalLayoutGroup>().spacing / 2;

        SetUpEvidenceElements();

        evidencePanelSize = (numberOfEvidenceToDisplay * evidenceButton.rectTransform.sizeDelta.x) + (numberOfEvidenceToDisplay - 1) * slider.GetComponent<HorizontalLayoutGroup>().spacing;

        float temp = (float)evidenceInHotbar.Count / (float)numberOfEvidenceToDisplay;
        amountOfScreens = Mathf.Clamp(Mathf.CeilToInt(temp), 1, 100);
        screenIndex = 0;
        slider.anchoredPosition = Vector2.zero;

        float sliderSize = evidencePanelSize * amountOfScreens + ((amountOfScreens - 1) * 5);

        slider.sizeDelta = new Vector2(sliderSize, buttonSize.y);
        evidencePanel.sizeDelta = new Vector2(evidencePanelSize, buttonSize.y);

        open = false;
    }

    private void SetUpEvidenceElements()
    {
        int indexToAssign = 0;

        foreach (Evidence evi in Engine.Instance.evidenceManager.gatheredEvidence.evidence)
        {

            if (EvidenceManager.Instance.gatheredEvidence.hiddenEvidence.Contains(evi))
                continue;

            EvidenceDisplay evidence = Instantiate(evidenceButton, slider);

            evidence.transform.SetAsFirstSibling();

            if (Engine.Instance.evidenceManager.gatheredEvidence.newEvidence.Contains(Engine.Instance.evidenceManager.evidence[evi.key]))
            {
                evidence.SetNotification(true);
            }

            evidence.Initialize(evi, indexToAssign);

            evidence.button.onDown.AddListener(() => SetDraggableIconFromIndex(evidence.indexofButton));
            evidence.button.onEnter.AddListener(() => DisplayEvidenceInfo(evidence));
            evidence.button.onExit.AddListener(() => HideEvidenceInfo());

            evidenceInHotbar.Add(evidence);

            indexToAssign++;
        }
    }

    public void DisplayEvidenceInfo(EvidenceDisplay display)
    {

        //if (isAnimating) return;

        evidenceInfoBubble.gameObject.SetActive(true);
        evidenceInfoBubbleTail.gameObject.SetActive(true);

        evidenceName.text = display.evidence.evidenceName;
        evidenceDescription.text = display.evidence.evidenceDescription;

        float infoBubblePosX = display.rectTransform.anchoredPosition.x + 30;

        float min = (evidencePanel.anchoredPosition.x - evidencePanel.sizeDelta.x) + (evidenceInfoBubble.sizeDelta.x / 2);
        float max = (evidencePanel.anchoredPosition.x) - (evidenceInfoBubble.sizeDelta.x / 2);

        evidenceInfoBubbleTail.anchoredPosition = new Vector2(infoBubblePosX, evidenceInfoBubbleTail.anchoredPosition.y);

        infoBubblePosX = Mathf.Clamp(infoBubblePosX, min, max);

        evidenceInfoBubble.anchoredPosition = new Vector2(infoBubblePosX, 50);

        popInfoTween.Kill();
        popInfoTween.Append(evidenceInfoBubble.DOAnchorPosY(75, 0.1f));

    }

    public void HideEvidenceInfo()
    {
        evidenceName.text = "";
        evidenceDescription.text = "";

        //evidenceInfoConstraint.RemoveSource(0);
        //evidenceInfoTailConstraint.RemoveSource(0);

        evidenceInfoBubble.gameObject.SetActive(false);
        evidenceInfoBubbleTail.gameObject.SetActive(false);
    }

    public void Event_LetGoOfDraggableIcon()
    {
        if (!draggableIcon.isDragging) return;

        if (overInspectionButton)
        {
            draggableIcon.hotbarItem.SetImageVisible(true);
            Engine.Instance.uiManager.uiEvidenceInspection.StartInspection(draggableIcon.evidence);
            CloseHotbar();
            draggableIcon.SetEmpty();
            return;
        }

        if (targetInteractable)
        {
            Player.Instance.GetMovementFunction().MoveToPresentToInteractable(targetInteractable, draggableIcon.evidence);
            targetInteractable = null;
            CloseHotbar();
        }
        else
        {
            AudioManager.PlaySfx(AudioDef.Instance.invalidDrop);
        }

        draggableIcon.hotbarItem.SetImageVisible(true);
        draggableIcon.SetEmpty();
    }

    public void Event_OnInspectEnter()
    {
        overInspectionButton = true;

        if (draggableIcon.isDragging)
        {
            inspectButtonGraphic.localScale = Vector3.one;
            inspectButtonGraphic.DOScale(Vector3.one * 1.1f, 0.2f);
        }
    }

    public void Event_OnInspectExit()
    {
        overInspectionButton = false;

        inspectButtonGraphic.localScale = Vector3.one;
    }

    public void SetDraggableIconFromIndex(int i)
    {
        draggableIcon.SetIcon(evidenceInHotbar[i]);
        evidenceInHotbar[i].SetImageVisible(false);

        if (evidenceInHotbar[i].newNotif)
        {
            Engine.Instance.evidenceManager.gatheredEvidence.newEvidence.Remove(Engine.Instance.evidenceManager.evidence[evidenceInHotbar[i].evidence.key]);
            evidenceInHotbar[i].SetNotification(false);
        }

    }

    public void ButtonEvent_OpenClose()
    {
        if (isAnimating) return;
        if (Player.Instance.GetMovementFunction().targetDestination.forced) return;

        if (open)
        {
            CloseHotbar();
            return;
        }

        OpenHotbar(openCloseCurve);
    }

    public void ButtonEvent_MoveLeft()
    {
        moveScreensTween.Kill();
        moveScreensTween = DOTween.Sequence();

        slider.anchoredPosition = new Vector2((screenIndex * -evidencePanelSize) - (screenIndex * 5), 0);

        if (screenIndex - 1 < 0)
        {
            moveScreensTween.Append(slider.DOPunchAnchorPos(Vector2.left * 10, 0.3f));
            moveScreensTween.Play();
            return;
        }
        screenIndex--;

        moveScreensTween.Append(slider.DOAnchorPosX((screenIndex * -evidencePanelSize) - (screenIndex * 5), 0.2f));
        moveScreensTween.Play();
    }

    public void ButtonEvent_MoveRight()
    {
        moveScreensTween.Kill();
        moveScreensTween = DOTween.Sequence();

        slider.anchoredPosition = new Vector2((screenIndex * -evidencePanelSize) - (screenIndex * 5), 0);

        if (screenIndex + 1 > amountOfScreens - 1)
        {
            moveScreensTween.Append(slider.DOPunchAnchorPos(Vector2.right * 10, 0.3f));
            moveScreensTween.Play();
            return;
        }

        screenIndex++;

        moveScreensTween.Append(slider.DOAnchorPosX((screenIndex * -evidencePanelSize) - (screenIndex * 5), 0.2f));
        moveScreensTween.Play();
    }

    public void OpenHotbar(AnimationCurve curve)
    {
        if (open) return;

        CalculateSizes();

        LayoutRebuilder.ForceRebuildLayoutImmediate(bar);

        overInspectionButton = false;
        open = true;
        AnimateOpen(curve);
        AudioManager.PlaySfx(openSfx);

        Event_OnEvidenceBarOpen.Invoke();
    }

    public void CloseHotbar(TweenCallback callback = null)
    {
        if (!open) return;

        open = false;
        AnimateClose();
        AudioManager.PlaySfx(closeSfx);
    }

    public void AnimateOpen(AnimationCurve curve)
    {
        isAnimating = true;

        openPosX = bar.sizeDelta.x + spacing;
        Vector2 openPos = new Vector2(openPosX, spacing);

        slideBarTween.Kill();
        slideBarTween = DOTween.Sequence();
        Tween openTween = bar.DOAnchorPosX(openPosX, 0.8f).SetEase(curve);
        slideBarTween.Append(openTween);
        slideBarTween.Play().OnComplete(OnOpenFinish);
    }

    public void AnimateClose(TweenCallback callback = null)
    {
        isAnimating = true;

        closePosX = buttonSize.x + spacing + 5;
        Vector2 closePos = new Vector2(closePosX, spacing);

        slideBarTween.Kill();
        slideBarTween = DOTween.Sequence();
        Tween closeTween = bar.DOAnchorPosX(closePosX, 0.8f).SetEase(openCloseCurve);
        slideBarTween.Append(closeTween);
        slideBarTween.Play().OnComplete(() =>
        {
            OnCloseFinish();
            callback?.Invoke();
        });
    }

    public bool IsOpen()
    {
        return open;
    }

    public void OnOpenFinish()
    {
        open = true;
        isAnimating = false;
    }

    public void OnCloseFinish()
    {
        open = false;
        isAnimating = false;

        for (int i = slider.childCount - 1; i >= 0; i--)
        {
            Destroy(slider.GetChild(i).gameObject);
        }

        evidenceInHotbar.Clear();
    }

    public void Event_AutoClose()
    {
        if (open)
            CloseHotbar();
    }

    public override void LoseFocus()
    {
        CloseHotbar();
    }

    public override void GainFocus()
    {

    }

}
