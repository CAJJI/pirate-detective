using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class UILoadingScreen : UILayer
{
    [HideInInspector] public UnityEvent Event_OnFadeOutComplete = new UnityEvent();
    [HideInInspector] public UnityEvent Event_OnFadeInComplete = new UnityEvent();

    public static UILoadingScreen Instance { get; private set; }
    public override string UIKey { get { return "UILoadingScreen"; } }

    public CanvasGroup overlay;

    public override void UI_Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public override void UI_Start() { }

    public override void UI_Update() { }

    public void FadeIn(float time, TweenCallback callback = null)
    {
        UIManager.Instance.SetFocus(this);

        overlay.alpha = 0;

        Sequence fadeInTween = DOTween.Sequence();

        fadeInTween.Append(overlay.DOFade(1, time));

        fadeInTween.Play().OnComplete(() =>
        {
            callback?.Invoke();
            Event_OnFadeInComplete.Invoke();
        });
    }

    public void FadeOut(float time, TweenCallback callback = null)
    {
        UIManager.Instance.SetFocus(this);

        overlay.alpha = 1;

        Sequence fadeOutTween = DOTween.Sequence();

        fadeOutTween.Append(overlay.DOFade(0, time));

        fadeOutTween.Play().OnComplete(() =>
        {
            callback?.Invoke();
            Event_OnFadeOutComplete.Invoke();
            UIManager.Instance.SetFocus("UIHUD");
        });
    }
}
