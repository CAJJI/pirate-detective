using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHUD : UILayer
{
    public override string UIKey { get { return "UIHUD"; } }


    public List<UILayer> HUDLayers;

    public override bool locked { get { return true; } }

    public Button mapButton;
    public Evidence unlockMapEvidence;

    public Button logButton;
    public DetectiveObjective unlockLogObjective;

    public CanvasGroup cg;

    public override void UI_Awake()
    {
        base.UI_Awake();

        foreach (UILayer layer in HUDLayers)
        {
            layer.gameObject.SetActive(true);
            layer.UI_Awake();
        }
    }

    public override void UI_Start()
    {
        PauseMenu.Instance.Event_PauseMenuOpen.AddListener(Event_OnPause);
        PauseMenu.Instance.Event_PauseMenuClose.AddListener(Event_OnUnpause);


        base.UI_Start();

        foreach (UILayer layer in HUDLayers)
        {
            layer.UI_Start();
        }

        Initialize();
    }

    public override void UI_Update()
    {
        base.UI_Update();

        foreach (UILayer layer in HUDLayers)
        {
            layer.UI_Update();
        }
    }

    public override void LoseFocus()
    {
        foreach (UILayer layer in HUDLayers)
        {
            layer.LoseFocus();
        }

        base.LoseFocus();

    }

    public override void GainFocus()
    {
        base.GainFocus();

        foreach (UILayer layer in HUDLayers)
        {
            layer.GainFocus();
        }
    }

    public override void SetInput()
    {
        InputManager.Instance.SetControllerMap(ControllerMap.Gameplay);
    }

    void Initialize()
    {
        if (!EvidenceManager.Instance.gatheredEvidence.HasGathered(unlockMapEvidence))
        {
            mapButton.gameObject.SetActive(false);
            EvidenceManager.Instance.gatheredEvidence.onEvidenceAdded.AddListener(OnEvidenceGathered);
        }

        InitializeLogButton();
    }

    private void InitializeLogButton()
    {
        logButton.onClick.AddListener(UILog.Instance.ButtonEvent_OpenLog);

        if (unlockLogObjective && !unlockLogObjective.IsComplete())
        {
            logButton.gameObject.SetActive(false);
            unlockLogObjective.Event_OnComplete.AddListener(() => { logButton.gameObject.SetActive(true); });
        }
    }

    void OnEvidenceGathered(Evidence evidence)
    {
        if (evidence == unlockMapEvidence)
        {
            mapButton.gameObject.SetActive(true);
            EvidenceManager.Instance.gatheredEvidence.onEvidenceAdded.RemoveListener(OnEvidenceGathered);
        }
    }

    private void Event_OnPause()
    {
        cg.alpha = 0;
    }
    private void Event_OnUnpause()
    {
        cg.alpha = 1;
    }
}
