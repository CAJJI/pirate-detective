using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class UIEvidenceInspection : UILayer
{
    public override string UIKey { get { return "UIEvidenceInspection"; } }
    public static UIEvidenceInspection Instance { get; private set; }

    [Header("References")]
    public RectTransform panel;
    public CanvasGroup panelCanvasGroup;
    public AnimationCurve animateCurve;
    public TextMeshProUGUI evidenceName;
    public TextMeshProUGUI evidenceDescription;
    public GameObject defaultObject;


    public RectTransform graphic;
    public Camera viewCam;
    public Transform point;
    public Transform debug;
    private bool exiting;

    [Header("Per Inspection")]
    public GameObject inspectionObject;

    private bool rotating;

    public override void UI_Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public override void UI_Start()
    {

        InputManager.Instance.MenuInputEvent_ConfirmDown.AddListener(Event_OnRotateStart);
        InputManager.Instance.MenuInputEvent_ConfirmDown.AddListener(Event_OnPress);
        InputManager.Instance.MenuInputEvent_ConfirmUp.AddListener(Event_OnRotateEnd);

    }

    public override void UI_Update()
    {
        if (!isInFocus) return;

        RotateObject();
    }

    private void Event_OnPress()
    {
        if (!isInFocus) return;

        point.DOKill();

        RaycastHit hit;
        Ray ray = viewCam.ViewportPointToRay(TransformMousePositionToUI());

        if (Physics.Raycast(ray, out hit))
        {
            InspectablePoint toInspect = hit.collider.gameObject.GetComponent<InspectablePoint>();
            debug.position = hit.point;
            if (toInspect)
            {
                toInspect.eventToDo.DoEvent();
                Debug.Log("Interacted with Inspectable Point");
            }
        }
    }

    Vector3 previousPos = Vector3.zero;
    Vector3 posDelta = Vector3.zero;

    public void Event_OnRotateStart()
    {
        if (!isInFocus) return;

        rotating = true;
    }

    public void Event_OnRotateEnd()
    {
        if (!isInFocus) return;

        rotating = false;
    }

    private void RotateObject()
    {
        if (rotating)
        {
            posDelta = Input.mousePosition - previousPos;

            if (Vector3.Dot(point.up, Vector3.up) >= 0)
            {
                point.Rotate(point.up, -Vector3.Dot(posDelta, viewCam.transform.right), Space.World);
            }
            else
            {
                point.Rotate(point.up, Vector3.Dot(posDelta, viewCam.transform.right), Space.World);
            }
            point.Rotate(Camera.main.transform.right, Vector3.Dot(posDelta, viewCam.transform.up), Space.World);
        }

        previousPos = Input.mousePosition;
    }

    public Vector3 TransformMousePositionToUI()
    {

        //This may possibly the worst thing I've ever done
        Vector2 coord = Vector2.zero;
        Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        //transform mouse pos to canvas space
        coord.x = mousePos.x / Screen.width * UIManager.Instance.scaler.referenceResolution.x;
        coord.y = mousePos.y / Screen.height * UIManager.Instance.scaler.referenceResolution.y;

        //get start/end points of 
        float xStart = (UIManager.Instance.scaler.referenceResolution.x / 2 - graphic.rect.width / 2) + graphic.anchoredPosition.x;
        float xEnd = xStart + graphic.rect.width + graphic.anchoredPosition.x;

        float yStart = (UIManager.Instance.scaler.referenceResolution.y / 2 - graphic.rect.height / 2) + graphic.anchoredPosition.y;
        float yEnd = yStart + graphic.rect.height + graphic.anchoredPosition.x + graphic.anchoredPosition.y;

        coord.x = Mathf.Clamp(coord.x, xStart, xEnd);
        coord.y = Mathf.Clamp(coord.y, yStart, yEnd);


        coord.x = (coord.x - xStart) / (graphic.rect.width);
        coord.y = (coord.y - yStart) / (graphic.rect.height);

        coord.x = Mathf.Clamp(coord.x, 0, 1);
        coord.y = Mathf.Clamp(coord.y, 0, 1);

        //Debug.Log(coord.x);
        //Debug.Log(coord.y);

        return new Vector3(coord.x, coord.y, 0);
    }

    public void StartInspection(Evidence evidence)
    {
        UIManager.Instance.SetFocus(this);
        exiting = false;
        rotating = false;
        ClearView();

        if (!evidence.evidenceObject)
        {
            inspectionObject = Instantiate(defaultObject, point, false);
        } else
        {
            inspectionObject = Instantiate(evidence.evidenceObject, point, false);
        }

        evidenceName.text = evidence.evidenceName;
        evidenceDescription.text = evidence.evidenceDescription;

        point.eulerAngles = evidence.startingRotation - (new Vector3(0,180,0));
        point.DORotate(evidence.startingRotation, 1);

        PanelTweenIn();
    }

    public void PanelTweenIn()
    {
        panel.anchoredPosition += Vector2.down * 400;
        panel.eulerAngles = new Vector3(0, 0, 80);
        panelCanvasGroup.alpha = 0;

        Sequence tweenIn = DOTween.Sequence();

        tweenIn.Append(panelCanvasGroup.DOFade(1, 0.5f));
        tweenIn.Join(panel.DOAnchorPosY(0, 0.5f).SetEase(animateCurve));
        tweenIn.Join(panel.DORotate(Vector3.zero, 0.5f).SetEase(animateCurve));
        tweenIn.Join(panel.DORotate(Vector3.zero, 0.5f).SetEase(animateCurve));

        tweenIn.Play();
    }

    public void PanelTweenOut(TweenCallback onComplete = null)
    {
        panel.anchoredPosition = Vector2.zero;
        panel.eulerAngles = new Vector3(0, 0, 0);
        panelCanvasGroup.alpha = 1;

        Sequence tweenIn = DOTween.Sequence();

        tweenIn.Append(panelCanvasGroup.DOFade(0, 0.5f));
        tweenIn.Join(panel.DOAnchorPosY(-400, 0.5f).SetEase(animateCurve));
        tweenIn.Join(panel.DORotate(new Vector3(0, 0, -40), 0.5f).SetEase(animateCurve));

        tweenIn.Play().OnComplete(onComplete);
    }

    private void ClearView()
    {
        if (point.childCount > 0)
        {
            foreach (Transform child in point)
            {
                Destroy(child.gameObject);
            }
        }
        point.eulerAngles = Vector3.zero;
    }

    public void Event_OnExit()
    {
        if (exiting) return;
        exiting = true;

        PanelTweenOut(() =>
        {
            UIManager.Instance.SetFocus("UIHUD");
            ClearView();
            UIHUDEvidenceBar.Instance.OpenHotbar(UIHUDEvidenceBar.Instance.openCloseCurveFlat);
        });
    }

    public override void LoseFocus()
    {
        base.LoseFocus();
    }
}
