using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStarter : MonoBehaviour
{

    public Music audioToStart;
    // Start is called before the first frame update
    void Start()
    {
        AudioManager.PlayMusic(audioToStart);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
