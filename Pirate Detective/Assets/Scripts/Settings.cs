using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Settings
{
    public float sfxVolume;
    public float musicVolume;
    public float speechVolume;

    public static Settings current { get { if (_current == null) { _current = defaultSettings; } return _current; } }
    static Settings _current = null;

    public static Settings defaultSettings { get { if (_defaultSettings == null) _defaultSettings = GetDefaultSettings(); return _defaultSettings; } }
    static Settings _defaultSettings = null;

    static Settings GetDefaultSettings()
    {
        Settings newSettings = new Settings()
        {
            sfxVolume = 1,
            musicVolume = 1,
            speechVolume = 1,
        };
        return newSettings;
    }
}
