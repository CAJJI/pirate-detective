using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTestDebug : MonoBehaviour
{
    public bool active = true;

    public List<DetectiveObjective> completedObjectives;
    public List<Evidence> evidenceToHave;

    public void LoadDebug()
    {

        SaveManager.Instance.transitionData.LoadDebug();

        foreach (DetectiveObjective obj in completedObjectives)
        {
            if (obj)
            obj.SetComplete();
        }

        foreach (Evidence evi in evidenceToHave)
        {
            if (evi)
                EvidenceManager.Instance.gatheredEvidence.AddEvidence(evi);
        }
    }
}