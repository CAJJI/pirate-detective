using UnityEngine;

[CreateAssetMenu(fileName = "NewObjectiveSaveData", menuName = "Pirate Detective/Saving/ObjectiveSaveData")]
public class ObjectiveSaveData : SaveData
{


    public override void Load()
    {
       
    }

    public override void Save()
    {
      
    }
    public override void Clear()
    {
        foreach (DetectiveObjective obj in ObjectiveManager.Instance.objectiveContainer.objectives)
        {
            obj.data.Reset();
        }

        ObjectiveManager.Instance.objectives.Clear();
    }
}
