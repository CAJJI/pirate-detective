using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "NewEvidenceSaveData", menuName = "Pirate Detective/Saving/EvidenceSaveData")]
public class EvidenceSaveData : SaveData
{

    public List<Evidence> gatheredEvidence = new List<Evidence>();
    public List<Evidence> newEvidence = new List<Evidence>();
    public List<Evidence> hiddenEvidence = new List<Evidence>();

    public override void Save()
    {
        gatheredEvidence = Engine.Instance.evidenceManager.gatheredEvidence.evidence;
        newEvidence = Engine.Instance.evidenceManager.gatheredEvidence.newEvidence;
        hiddenEvidence = Engine.Instance.evidenceManager.gatheredEvidence.hiddenEvidence;
    }

    public override void Load()
    {
        Engine.Instance.evidenceManager.gatheredEvidence.evidence.Clear();
        Engine.Instance.evidenceManager.gatheredEvidence.evidence = gatheredEvidence;
        
        Engine.Instance.evidenceManager.gatheredEvidence.newEvidence.Clear();
        Engine.Instance.evidenceManager.gatheredEvidence.newEvidence = newEvidence;
        
        Engine.Instance.evidenceManager.gatheredEvidence.hiddenEvidence.Clear();
        Engine.Instance.evidenceManager.gatheredEvidence.hiddenEvidence = hiddenEvidence;
    }

    public override void Clear()
    {
        gatheredEvidence.Clear();
        newEvidence.Clear();
        hiddenEvidence.Clear();
    }
}
