
using UnityEngine;

public abstract class SaveData : ScriptableObject
{
    public abstract void Save();
    public abstract void Load();
    public abstract void Clear();

}
