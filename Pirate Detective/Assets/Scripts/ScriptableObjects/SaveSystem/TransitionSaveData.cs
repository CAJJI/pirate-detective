using UnityEngine;

[CreateAssetMenu(fileName = "NewTransitionSaveData", menuName = "Pirate Detective/Saving/TransitionSaveData")]
public class TransitionSaveData : SaveData
{

    public string sceneName;
    public string entranceName;

    public bool firstLoad = true;

    public override void Save()
    {
        sceneName = SceneTransitionManager.Instance.targetScene;
        entranceName = SceneTransitionManager.Instance.targetEntrance;
    }

    public override void Load()
    {
        SceneTransitionManager.Instance.targetScene = sceneName;
        SceneTransitionManager.Instance.targetEntrance = entranceName;
        firstLoad = false;
    }

    public void LoadDebug()
    {
        firstLoad = false;
    }

    public override void Clear()
    {
        sceneName = "";
        entranceName = "";
        firstLoad = true;
    }

}
