using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character Properties", menuName = "Pirate Detective/Character Property")]
public class CharacterProperties : ScriptableObject
{
    public float speed;
    public float interactionDistance;
}
