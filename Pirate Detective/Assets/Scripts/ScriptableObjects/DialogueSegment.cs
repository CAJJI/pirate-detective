using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newDialogueSegment", menuName = "Pirate Detective/Dialogue Segment")]
public class DialogueSegment : ScriptableObject
{
    public string comment;
    public List<DialogueNode> nodes = new List<DialogueNode>();
    public void AddNode(DialogueNode node)
    {
        nodes.Add(node);
    }
}
