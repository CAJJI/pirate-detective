using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewLogData", menuName = "Pirate Detective/Log/Log Data")]
public class LogData : ScriptableObject
{
    [System.Serializable]
    public class LogCharacter
    {
        public string name;
        public Sprite icon;
        public DetectiveObjective revealOnComplete;
        public List<LogEntry> logEntries;
    }

    public List<LogCharacter> characters;
    public int startTime = 10;
    public int amountOfHours;

}
