using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewLogEntry", menuName = "Pirate Detective/Log/Log Entry")]
public class LogEntry : ScriptableObject
{
    public bool isNull = false;
    public DetectiveObjective revealOnComplete;
    [TextArea]
    public string description;
    [Space]
    public DetectiveObjective updatesIntoOn;
    public LogEntry updatesInto;

    public LogEntry GetCurrentLogEntry()
    {
        if (updatesInto && updatesIntoOn.IsComplete())
        {
            return updatesInto.GetCurrentLogEntry();
        }

        return this;
    }

}
