using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

[CustomEditor(typeof(LogData))]
public class LogDataEditor : Editor
{


    public override void OnInspectorGUI()
    {
        LogData target = (LogData)this.target;

        DrawDefaultInspector();

        AmountOfHoursNotSynced(target);

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();

        GUILayout.Label("Name", GUILayout.Width(50));

        foreach (LogData.LogCharacter character in target.characters)
        {
            GUILayout.Label(character.name, GUILayout.Width(100));
        }

        GUILayout.EndHorizontal();


        for (int i = 0; i < target.amountOfHours; i++)
        {
            GUILayout.BeginHorizontal();

            int val = i + target.startTime;
            TimeSpan result = TimeSpan.FromHours(val);
            DateTime time = DateTime.Today.Add(result);
            string displayTime = time.ToString("hh tt");
            GUILayout.Label(displayTime, GUILayout.Width(50));

            foreach (LogData.LogCharacter character in target.characters)
            {
                character.logEntries[i] = EditorGUILayout.ObjectField(character.logEntries[i], typeof(LogEntry), false, GUILayout.Width(100)) as LogEntry;
            }

            GUILayout.EndHorizontal();

        }

        GUILayout.EndVertical();

        EditorUtility.SetDirty(target);

    }

    private static void AmountOfHoursNotSynced(LogData target)
    {
        if (target.characters.Count > 0 && target.characters[0].logEntries.Count != target.amountOfHours)
        {

            foreach (LogData.LogCharacter character in target.characters)
            {
                List<LogEntry> tempLog = new List<LogEntry>();

                for (int i = 0; i < target.amountOfHours; i++)
                {
                    if (character.logEntries.Count > 0 && i <= character.logEntries.Count - 1)
                    {
                        tempLog.Add(character.logEntries[i]);
                    }
                    else
                    {
                        tempLog.Add(null);
                    }

                }
                character.logEntries = tempLog;
            }
        }
    }
}

