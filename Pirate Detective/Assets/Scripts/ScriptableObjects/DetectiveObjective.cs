using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "New Detective Objective", menuName = "Pirate Detective/Detective Objective")]
public class DetectiveObjective : ScriptableObject
{
    public string key { get { return name; } }
    public string descriptiveName;

    [TextArea]
    public string notes;

    [HideInInspector] public UnityEvent Event_OnComplete = new UnityEvent();

    public ObjectiveData data=new ObjectiveData();

    public class ObjectiveData
    {
        private bool completed;
        public bool Completed { get { return completed; } }
        public void SetCompleted()
        {
            completed = true;
        }

        public void Reset()
        {
            completed = false;
        }
    }

    public void SetComplete()
    {
        if (IsComplete()) return;

        data.SetCompleted();

        Event_OnComplete.Invoke();
    }

    public bool IsComplete()
    {
        if (data == null) return false;
        return data.Completed;
    }
}
