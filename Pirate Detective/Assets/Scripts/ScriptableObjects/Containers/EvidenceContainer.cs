using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EvidenceContainer", menuName = "Pirate Detective/Evidence Container")]
public class EvidenceContainer : ScriptableObject
{
    public string evidenceFolderPath;

    public List<Evidence> evidence = new List<Evidence>();


}
