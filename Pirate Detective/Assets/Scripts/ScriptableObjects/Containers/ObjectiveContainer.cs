using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ObjectiveContainer", menuName = "Pirate Detective/Objective Container")]
public class ObjectiveContainer : ScriptableObject
{
    public string objectiveFolderPath;

    public List<DetectiveObjective> objectives = new List<DetectiveObjective>();

    public void PopulateObjectivesFromProject()
    {
        objectives.Clear();
        //Populate objectives
    }
}
