using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ObjectiveContainer))]
public class ObjectiveContainerEditor : Editor
{


    public override void OnInspectorGUI()
    {
        ObjectiveContainer target = (ObjectiveContainer)this.target;

        if (GUILayout.Button("Populate From Path"))
        {
            target.objectives.Clear();
            string[] guids = AssetDatabase.FindAssets("t:DetectiveObjective", new[] { target.objectiveFolderPath });
            serializedObject.FindProperty("objectives").arraySize = guids.Length;
            for (int i = 0; i < guids.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                DetectiveObjective o = (DetectiveObjective)AssetDatabase.LoadAssetAtPath(path, typeof(DetectiveObjective));
                serializedObject.FindProperty("objectives").GetArrayElementAtIndex(i).objectReferenceValue = o;
            }

            serializedObject.ApplyModifiedProperties();
            Debug.Log("Total of " + target.objectives.Count + " items added");
        }

        if (GUILayout.Button("Validate Objective Keys"))
        {
            DetectiveObjective targetObjectives;
            List<DetectiveObjective> objectiveseWithIssues = new List<DetectiveObjective>();

            foreach (DetectiveObjective objectives in target.objectives)
            {
                if (objectiveseWithIssues.Contains(objectives))
                    continue;

                targetObjectives = objectives;

                foreach (DetectiveObjective objectivesToCheck in target.objectives)
                {
                    if (targetObjectives == objectivesToCheck)
                        continue;

                    if (objectiveseWithIssues.Contains(objectivesToCheck))
                        continue;

                    if (targetObjectives.key == objectivesToCheck.key)
                    {
                        Debug.Log(targetObjectives.descriptiveName + " and " + objectivesToCheck.descriptiveName + " have the same key (evidenceName)", targetObjectives);
                        objectiveseWithIssues.Add(targetObjectives);
                        objectiveseWithIssues.Add(objectivesToCheck);
                    }
                }


            }

            Debug.Log("Total of " + objectiveseWithIssues.Count + " Issues");

        }

        DrawDefaultInspector();
    }



}
