using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EvidenceContainer))]
public class EvidenceContainerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EvidenceContainer target = (EvidenceContainer)this.target;

        if (GUILayout.Button("Populate From Path"))
        {
            target.evidence.Clear();
            string[] guids = AssetDatabase.FindAssets("t:Evidence", new[] { target.evidenceFolderPath });
            serializedObject.FindProperty("evidence").arraySize = guids.Length;
            for (int i = 0; i < guids.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                Evidence e = (Evidence)AssetDatabase.LoadAssetAtPath(path, typeof(Evidence));
                //target.evidence.Add(e);
                serializedObject.FindProperty("evidence").GetArrayElementAtIndex(i).objectReferenceValue = e;
            }

            serializedObject.ApplyModifiedProperties();
            Debug.Log("Total of " + target.evidence.Count + " items added");
        }

        if (GUILayout.Button("Validate Evidence Keys"))
        {
            Evidence targetEvidence;
            List<Evidence> evidenceWithIssues = new List<Evidence>();

            foreach (Evidence evidence in target.evidence)
            {
                if (evidenceWithIssues.Contains(evidence))
                    continue;

                targetEvidence = evidence;

                foreach (Evidence evidenceToCheck in target.evidence)
                {
                    if (targetEvidence == evidenceToCheck)
                        continue;

                    if (evidenceWithIssues.Contains(evidenceToCheck))
                        continue;

                    if (targetEvidence.key == evidenceToCheck.key)
                    {
                        Debug.Log(targetEvidence.evidenceName + " and " + evidenceToCheck.evidenceName + " have the same key (evidenceName)", targetEvidence);
                        evidenceWithIssues.Add(targetEvidence);
                        evidenceWithIssues.Add(evidenceToCheck);
                    }
                }


            }

            Debug.Log("Total of " + evidenceWithIssues.Count + " Issues");

        }

        DrawDefaultInspector();
    }



}
