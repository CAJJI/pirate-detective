using UnityEngine;

[CreateAssetMenu(fileName = "NewEvidence", menuName = "Pirate Detective/Evidence")]
public class Evidence : ScriptableObject
{
    public string key { get { return name; } }
    public string evidenceName;
    public bool  testimony;
    [TextArea]
    public string evidenceDescription;
    public GameObject evidenceObject;
    public Sprite evidenceImage;
   // public bool setStartingRotation;
    public Vector3 startingRotation; //for focusing on sub pieces of evidence if you investiage them, since the same model is used. 
}
