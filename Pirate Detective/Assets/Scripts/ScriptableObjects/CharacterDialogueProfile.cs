using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

[CreateAssetMenu(fileName = "NewCharacterDialogueProfile", menuName = "Pirate Detective/Character Dialogue Profile")]
public class CharacterDialogueProfile : ScriptableObject
{
    public string characterName;

    public Sprite icon;

    public SpeechNote rootSpeechNote;
    public int vocalExclamationIncrease = 4;

    public TMP_FontAsset font;

    public Color dialogueBoxInnerColor = Color.white;
    public Color dialogueBoxOuterColor = Color.gray;
    public Color fontColor = Color.black;

    [TextArea]
    public string description;

    [System.Serializable]
    public class DialogueState
    {
        public DetectiveObjective ifNotComplete;

        [Space]
        public DialogueSegment dialogueSegment;
        public Evidence evidenceToProgress;

        [Space]
        public DetectiveObjective completeOnDialogue;
        public DetectiveObjective completeOnEvidence;

        [Space]
        public bool onPresentEvidence;
        public Evidence evidencePresented;

        [Space]
        public bool resetToDefaultMusic;

        [Space]
        public string sceneTransition;

    }

    public List<DialogueState> dialogueStates = new List<DialogueState>();
    public DialogueState irrelevantEvidenceState;

    public DialogueState GetCurrentDialogState()
    {
        foreach (DialogueState state in dialogueStates)
        {            
            if (state.ifNotComplete != null && ObjectiveManager.Instance.objectives[state.ifNotComplete.key].IsComplete()) 
                continue;

            return state;
        }

        return null;
    }

    public DialogueState GetEvidenceDialogState(Evidence evidence)
    {
        foreach (DialogueState state in dialogueStates)
        {
            if (state.onPresentEvidence && state.evidencePresented == evidence)
                return state;            
        }

        return null;
    }
}
