using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class EndController : MonoBehaviour
{
    //fade from black
    //picture of murder
    //play stinger
    //after x seconds, fade to black
    //credits
    //button to return to main menu
    Animator animator;

    public Sfx[] sfxs;

    bool canContinue;

    private void Start()
    {
        animator = GetComponent<Animator>();
        AudioManager.StopMusic();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (canContinue)
            {
                animator.SetTrigger("continue");
                canContinue = false;
            }
        }
    }

    public void AllowContinue()
    {
        canContinue = true;
    }

    public void PlaySfx(int index)
    {
        AudioManager.PlaySfx(sfxs[index]);
    }

    public void GoToMainMenu()
    {        
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
}
