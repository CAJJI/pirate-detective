using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneForceLoader : MonoBehaviour
{

    public DetectiveObjective objectiveToCheck;
    public string sceneToLoad;

    void Awake()
    {
        if (objectiveToCheck.IsComplete())
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}
