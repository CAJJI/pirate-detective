using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjectSingleton<T> : ScriptableObject where T : ScriptableObject
{
    const string DEFS_PATHS = "Defs/";

    public static T Instance { get { if (_instance == null) _instance = Resources.Load<T>(DEFS_PATHS + typeof(T).ToString()); return _instance; } }
    static T _instance;
}
