using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceClearSaveData : MonoBehaviour
{

    public TransitionSaveData transitionData;
    public EvidenceSaveData evidenceData;
    public ObjectiveSaveData objectiveData;

    public void Awake()
    {
        transitionData.Clear();
        evidenceData.Clear();
        objectiveData.Clear();
    }


}
