using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MapLocation : MonoBehaviour
{
    public string sceneName;
    public string entranceName;
    public string[] alternateIfCurrentSceneName;
    public string alternateEntranceName;
    public DetectiveObjective requiredObjective;
    public DetectiveObjective lockOnObjective;
    public Button button;
    bool locked;

    public void UpdateState()
    {        
        if (requiredObjective != null)
        {
            locked = !ObjectiveManager.Instance.objectives[requiredObjective.key].IsComplete();
        }
        else
        {
            locked = false;
        }

        if (lockOnObjective)
        {
            if (ObjectiveManager.Instance.objectives[lockOnObjective.key].IsComplete())
                locked = true;
        }

        button.interactable = !locked;        
    }

    public void Select()
    {
        Scene activeScene = SceneManager.GetActiveScene();
        string entrance = entranceName;

        foreach(string name in alternateIfCurrentSceneName)
        {
            if (activeScene.name == name)
            {
                entrance = alternateEntranceName;
                break;
            }
        }        

        if (activeScene.name == sceneName)
        {
            //already in scene
            Map.Instance.Close();
        }
        else
        {            
            Map.Instance.Close();
            SceneTransitionManager.Instance.SetSceneTransition(sceneName, entrance);
        }
    }
}
