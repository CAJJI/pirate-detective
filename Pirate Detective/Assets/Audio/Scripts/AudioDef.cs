using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioDef", menuName = "Pirate Detective/Audio/AudioDef")]
public class AudioDef : ScriptableObjectSingleton<AudioDef>
{
    public int vocalRange = 4;    
    public CharacterDialogueProfile playerDialogueProfile;
    public int playerVocalExclamationIncrease { get { return playerDialogueProfile.vocalExclamationIncrease; } }
    public SpeechNote playerRootSpeechNote { get { return playerDialogueProfile.rootSpeechNote; } }

    public Music mainmenuMusic;
    public Music gameplayMusic;
    public Music intenseGameplayMusic;

    public float sfxOffset;

    public float musicVolumeMultiplier = 1;
    public float sfxVolumeMultiplier = 1;
    public float speechVolumeMultiplier = 1;

    public Sfx evidenceAdded;
    public Sfx evidencePresented;
    public Sfx invalidDrop;
}
