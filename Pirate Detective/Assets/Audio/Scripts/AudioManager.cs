using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpeechNote
{
    C5,
    C5s,
    D5,
    D5s,
    E5,
    F5,
    F5s,
    G5,
    G5s,
    A5,
    A5s,
    B5,    
    C6,
    C6s,
    D6,
    D6s,
    E6,   
    F6,
    F6s,
    G6,
    G6s,
    A6,
    A6s,
    B6,
    C7,
    C7s,
    D7,
    D7s,
    E7,
    F7,
    F7s,
    G7,
    G7s,
    A7,
    A7s,
    B7,
}

public class AudioManager : Singleton<AudioManager>
{
    const string SPEECH_PATH = "Audio/Speech/";
    const string SFX_PATH = "Audio/Sfx/";
    const string MUSIC_PATH = "Audio/Music/";

    public int sfxLimit = 20;

    public AudioSource musicAudioSource;
    public AudioSource speechAudioSource;
    [HideInInspector]
    public List<AudioSource> sfxAudioSources = new List<AudioSource>();

    Music currentMusic;

    private void Awake()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        Initialize();
    }

    void Initialize()
    {        
        sfxAudioSources.Clear();
        for (int i = 0; i < sfxLimit; i++)
        {
            GameObject sfx = new GameObject("sfx_" + i);
            sfx.transform.SetParent(transform);
            sfxAudioSources.Add(sfx.AddComponent<AudioSource>());
        }

        UpdateSettings(Settings.current);
    }

    private void Update()
    {
        UpdateSpeech();
    }

    void UpdateSpeech()
    {
        AudioSource source = speechAudioSource;
        if (!source)
            return;
        if (source.isPlaying)
        {
            float volume = source.volume;
            float progress = (source.time / source.clip.length);
            if (progress < 0.5f)
                volume = (source.time / source.clip.length);
            else
                volume = 1 - (source.time / source.clip.length);

            source.volume = GetSpeechVolume(volume);
        }
    }
    
    public void UpdateSettings(Settings settings)
    {
        SetSfxVolume(settings.sfxVolume);
        SetMusicVolume(settings.musicVolume);
        SetSpeechVolume(settings.speechVolume);
    }
    public void SetSfxVolume(float value)
    {
        foreach(AudioSource source in sfxAudioSources)
        {
            source.volume = value * AudioDef.Instance.sfxVolumeMultiplier;            
        }
    }
    public void SetMusicVolume(float value)
    {
        musicAudioSource.volume = value * AudioDef.Instance.musicVolumeMultiplier;        
    }
    public void SetSpeechVolume(float value)
    {
        speechAudioSource.volume = value * AudioDef.Instance.speechVolumeMultiplier;
    }

    public static AudioSource PlaySfx(Sfx sfx)
    {
        if (sfx == null)
        {            
            return null;
        }
        foreach(AudioSource source in Instance.sfxAudioSources)
        {
            if (!source.isPlaying)
            {
                AudioClip clip = GetSfxClip(sfx.name);
                source.clip = clip;
                source.volume = GetSfxVolume(sfx);
                source.Play();
                source.time = AudioDef.Instance.sfxOffset;

                return source;
            }
        }
        return null;
    }
    
    public static void PlayMusic(Music music)
    {
        if (music == null)
        {            
            return;
        }

        AudioManager.Instance.currentMusic = music;

        AudioSource source = Instance.musicAudioSource;
        AudioClip clip = GetMusicClip(music.name);
        source.clip = clip;
        source.Play();
    }

    public static void StopMusic()
    {
        Instance.musicAudioSource.Stop();
    }

    public static void PlaySpeech(SpeechNote note)
    {        
        AudioSource source = Instance.speechAudioSource;
        AudioClip clip = GetSpeechClip(note.ToString());
        if (clip == null || source.isPlaying == true)
        {            
            return;
        }
        source.clip = clip;
        source.Play();        
    }


    public Music GetCurrentMusicFile()
    { 
        return currentMusic;
    }

    static float GetSpeechVolume(float value)
    {
        return value * Settings.current.speechVolume * AudioDef.Instance.speechVolumeMultiplier;
    }

    static float GetSfxVolume(Sfx sfx)
    {
        return sfx.volume * Settings.current.sfxVolume * AudioDef.Instance.sfxVolumeMultiplier;
    }

    static AudioClip GetSfxClip(string name)
    {
        return Resources.Load<AudioClip>(SFX_PATH + name);
    }

    static AudioClip GetMusicClip(string name)
    {
        return Resources.Load<AudioClip>(MUSIC_PATH + name);
    }

    static AudioClip GetSpeechClip(string name)
    {
        return Resources.Load<AudioClip>(SPEECH_PATH + name);
    }
}
