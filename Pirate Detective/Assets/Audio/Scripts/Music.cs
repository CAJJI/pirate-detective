using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Music", menuName = "Pirate Detective/Audio/Music")]
public class Music : ScriptableObject
{
    public string notes;
}