using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(UIAudioPlayer))]
public class UIAudioPlayerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }
}
#endif

public class UIAudioPlayer : EventTrigger
{
    public enum UIAction
    {
        OnMouseDown,
        OnMouseUp,
        OnMouseEnter,
        OnMouseExit,        
        OnBeginDrag,
    }

    [System.Serializable]
    public class UIAudioAction
    {
        public UIAction action;
        public Sfx sfx;
    }

    public List<UIAudioAction> audioActions = new List<UIAudioAction>();

    public override void OnBeginDrag(PointerEventData eventData)
    {
        PlaySfx(UIAction.OnBeginDrag);
        base.OnBeginDrag(eventData);
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        PlaySfx(UIAction.OnMouseDown);
        base.OnPointerDown(eventData);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        PlaySfx(UIAction.OnMouseUp);
        base.OnPointerUp(eventData);
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        PlaySfx(UIAction.OnMouseEnter);
        base.OnPointerEnter(eventData);
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        PlaySfx(UIAction.OnMouseExit);
        base.OnPointerExit(eventData);
    }

    public void PlaySfx(UIAction action)
    {
        foreach(UIAudioAction audioAction in audioActions)
        {
            if (audioAction.action == action)
            {
                AudioManager.PlaySfx(audioAction.sfx);
            }
        }
    }
}
