using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Sfx", menuName = "Pirate Detective/Audio/Sfx")]
public class Sfx : ScriptableObject
{
    public float volume = 1;
}