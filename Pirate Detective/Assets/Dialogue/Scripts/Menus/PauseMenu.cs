using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PauseMenu : MenuBase<PauseMenu>
{

    [HideInInspector] public UnityEvent Event_PauseMenuOpen = new UnityEvent();
    [HideInInspector] public UnityEvent Event_PauseMenuClose = new UnityEvent();

    public void OpenSettings()
    {
        Close();
        SettingsMenu.Instance.Open();
    }

    public void Quit()
    {
        ConfirmationMenu.Open("Are you sure you want to quit?", "Quit", "Cancel", () => { Application.Quit(); });
        Close();
    }

    public override void Open()
    {
        base.Open();
        Event_PauseMenuOpen.Invoke();
    }
    public override void Close()
    {
        base.Close();
        Event_PauseMenuClose.Invoke();
    }

}


