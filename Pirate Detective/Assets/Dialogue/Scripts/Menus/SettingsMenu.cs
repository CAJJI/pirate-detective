using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SettingsMenu : MenuBase<SettingsMenu>
{    
    Settings settings;

    public Slider sfxVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider speechVolumeSlider;
    bool dirty;

    public override void Open()
    {
        base.Open();
        Initialize();
    }

    void Initialize()
    {
        settings = Settings.current;        

        sfxVolumeSlider.value = settings.sfxVolume;
        musicVolumeSlider.value = settings.musicVolume;
        speechVolumeSlider.value = settings.speechVolume;        
    }

    private void LateUpdate()
    {
        if (dirty)
        {
            UpdateSettings();
            SetDirty(false);
        }
    }

    public override void Close()
    {
        base.Close();            
        if (PauseMenu.Instance)
        {
            PauseMenu.Instance.Open();
        }
    }

    public void UpdateSfxVolume(float value)
    {
        settings.sfxVolume = value;
        SetDirty(true);
    }

    public void UpdateMusicVolume(float value)
    {
        settings.musicVolume = value;
        SetDirty(true);
    }

    public void UpdateSpeechVolume(float value)
    {
        settings.speechVolume = value;
        SetDirty(true);
    }

    void UpdateSettings()
    {
        AudioManager.Instance.UpdateSettings(settings);
    }

    void SetDirty(bool state)
    {
        dirty = state;
    }
}
