using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBase<T> : Singleton<T> where T : MonoBehaviour
{    
    public GameObject container;

    public bool isOpen { get { return container.activeSelf; } }

    public static bool MenuIsOpen()
    {        
        foreach (MenuBase<MonoBehaviour> menu in FindObjectsOfType<MenuBase<MonoBehaviour>>())
        {
            if (menu.isOpen)
                return true;
        }
        return false;
    }

    public virtual void Open()
    {
        container.SetActive(true);
    }

    public virtual void Close()
    {
        container.SetActive(false);
    }
}
