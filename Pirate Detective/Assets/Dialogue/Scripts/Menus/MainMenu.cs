using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainMenu : MenuBase<MainMenu>
{
    public Music mainMenuMusic;
    public Music gameplayMusic;
    public string startGameScene = "Ship";

    public GameObject creditsContainer;
    public Image fadeScreen;
    public float fadeDuration = 2;
    
    private void Start()
    {
        AudioManager.PlayMusic(mainMenuMusic);
    }

    public void StartGame()
    {                
        fadeScreen.gameObject.SetActive(true);
        fadeScreen.DOFade(1, fadeDuration).OnComplete(()=> {
            UnityEngine.SceneManagement.SceneManager.LoadScene(startGameScene);
            AudioManager.PlayMusic(gameplayMusic);
        });
    }    

    public void OpenCredits()
    {
        creditsContainer.SetActive(true);
    }

    public void CloseCredits()
    {
        creditsContainer.SetActive(false);
    }

    public void OpenSettings()
    {
        SettingsMenu.Instance.Open();
    }

    public void Quit()
    {
        Application.Quit();
    }
}
