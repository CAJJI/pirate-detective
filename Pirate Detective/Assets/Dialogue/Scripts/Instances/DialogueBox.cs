using TMPro;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBox : Singleton<DialogueBox>
{
    public int vocalRange = 4;
    public int vocalExclamationIncrease = 2;

    public bool hidden;

    public float charactersPerSecond = 10;
    public bool pauseOnPunctuations = true;
    string dialogue;
    public bool isTypingText;
    float currentTypingDelay;
    float timeSinceLastCharacter;    

    public TextMeshProUGUI dialogueText;    
    
    Action onComplete;    

    List<char> punctuations = new List<char>()
    {
        '.', '!', '?', '~', '-',
    };
    bool previouslyPunctuated;

    public CharacterDialogueFile activeSpeaker;
    bool isPlayer;

    private void Update()
    {        
        UpdateText();        
    }    

    void UpdateText()
    {
        if (isTypingText)
        {
            //if (GameManager.isPaused) return;

            if (currentTypingDelay > 0)
            {
                currentTypingDelay -= Time.deltaTime;
                return;
            }

            int addCharacterCount = Mathf.FloorToInt(Time.deltaTime * charactersPerSecond);

            if (addCharacterCount <= 0)
            {
                timeSinceLastCharacter += Time.deltaTime;
                addCharacterCount = Mathf.FloorToInt(timeSinceLastCharacter * charactersPerSecond);
                if (addCharacterCount <= 0)
                    return;
            }

            timeSinceLastCharacter = 0;

            string currentText = dialogueText.text;

            if (currentText.Length + addCharacterCount > dialogue.Length)
                addCharacterCount = dialogue.Length - currentText.Length;

            string difference = dialogue.Substring(currentText.Length, addCharacterCount);

            if (pauseOnPunctuations)
            {
                char punctuation = 'x';
                int activeIndex = 0;
                for (int i = 0; i < punctuations.Count; i++)
                {
                    int index = difference.IndexOf(punctuations[i]);
                    if (index >= 0 && index <= activeIndex)
                    {
                        punctuation = punctuations[i];
                    }
                }
                if (punctuation != 'x')
                {
                    difference = difference.Substring(0, difference.IndexOf(punctuation) + 1);
                    if (previouslyPunctuated && punctuations.Contains(difference[0]))
                        currentTypingDelay = 0.01f;
                    else
                        currentTypingDelay = 0.2f;

                    previouslyPunctuated = true;
                }
                else
                {
                    previouslyPunctuated = false;
                }
            }
            else
            {
                previouslyPunctuated = false;
            }

            dialogueText.text += difference;

            PlaySfx(currentText, dialogue);

            if (dialogueText.text == dialogue)
            {
                isTypingText = false;
            }
        }
    }        

    public void SetSpeaker(CharacterDialogueFile characterDialogueFile)
    {
        activeSpeaker = characterDialogueFile;
    }

    public void SetIsPlayer(bool state)
    {
        isPlayer = state;
    }
    
    public void ClearText()
    {
        SetText("");
    }    

    public void SetText(string text)
    {
        dialogue = text;

        Debug.Log("Set Text: " + text);
        dialogueText.text = "";
        isTypingText = true;
    }

    public void CompleteTypingText()
    {
        isTypingText = false;
        dialogueText.text = dialogue;
    }

    public void OnEnd()
    {
        onComplete?.Invoke();
        onComplete = null;
    }

    void PlaySfx(string currentText, string fullText)
    {
        //play random 4 notes from root note
        //if the next punctuation in the remaining text is an exclamation mark, shift notes up 2

        string remainingText = fullText.Remove(0, currentText.Length);

        if (remainingText.Length == 0 || punctuations.Contains(remainingText[0]) || remainingText[0] == ' ')
            return;

        bool isExclamation = false;
        for (int i = 0; i < remainingText.Length; i++)
        {
            if (punctuations.Contains(remainingText[i]))
            {
                if (remainingText[i] == '!')
                    isExclamation = true;
                break;
            }
        }

        SpeechNote note = AudioDef.Instance.playerRootSpeechNote;
        if (activeSpeaker != null && !isPlayer)
        {
            note = activeSpeaker.rootSpeechNote;
        }
        int value = remainingText[0];        
        value %= vocalRange;
        note += value;

        if (isExclamation)
            note += vocalExclamationIncrease;

        AudioManager.PlaySpeech(note);        
    }    
}
