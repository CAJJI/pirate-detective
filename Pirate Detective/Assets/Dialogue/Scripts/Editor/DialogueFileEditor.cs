using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
using UnityEditorInternal;

    [CustomEditor(typeof(DialogueFile))]
    public class DialogueFileEditor : EditorTool
    {
        float padding = 3;
        float indentPadding = 5;
        ReorderableList nodesList;

        public override void OnInspectorGUI()
        {
            DialogueFile target = (DialogueFile)this.target;

            if (CreateButton("Toggle Comments", Color.white))
            {
                bool state = EditorPrefs.GetBool("ShowStoryNodeComments");
                EditorPrefs.SetBool("ShowStoryNodeComments", !state);
            }

            if (CreateButton("Expand Nodes", Color.white))
            {
                ExpandNodes(true);
            }

            if (CreateButton("Collapse Nodes", Color.white))
            {
                ExpandNodes(false);
            }

            SerializedProperty comment = serializedObject.FindProperty("comment");
            comment.stringValue = EditorGUILayout.TextArea(comment.stringValue);

            nodesList.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }

        public void ExpandNodes(bool state)
        {
            SerializedProperty nodes = serializedObject.FindProperty("nodes");
            for (int i = 0; i < nodes.arraySize; i++)
            {
                nodes.GetArrayElementAtIndex(i).isExpanded = state;
            }
        }

        #region ReorderableList
        private void OnEnable()
        {
            DialogueFile target = (DialogueFile)this.target;

            SerializedProperty nodes = serializedObject.FindProperty("nodes");

            nodesList = new ReorderableList(serializedObject, nodes, true, true, true, true);

            nodesList.drawHeaderCallback = (Rect rect) =>
            {
                EditorGUI.LabelField(rect, "Nodes");
            };

            nodesList.onAddCallback = (ReorderableList list) =>
            {
                nodes.InsertArrayElementAtIndex(list.count);
                nodes.GetArrayElementAtIndex(list.count - 1).isExpanded = true;
                serializedObject.ApplyModifiedProperties();
            };

            nodesList.elementHeightCallback = (int index) =>
            {
                float height = singleLine + padding;
                int rows = 2;
                if (index >= nodes.arraySize) return height;

                SerializedProperty element = nodes.GetArrayElementAtIndex(index);
                SerializedProperty nodeType = element.FindPropertyRelative("nodeType");
                if (element.isExpanded)
                {
                    if (nodeType.enumValueIndex == (int)NodeType.Dialogue)
                    {
                        rows += 5;
                    }
                    //else if (nodeType.enumValueIndex == (int)NodeType.Sfx)
                    //    rows += 4;
                }
                return 10 + (height * rows);
            };

            nodesList.drawElementBackgroundCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                if (index % 2 == 0)
                    EditorGUI.DrawTextureAlpha(rect, CreateTexture(2, 2, Color.white / 3), ScaleMode.ScaleAndCrop);
            };

            nodesList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                if (index >= nodes.arraySize) return;

                SerializedProperty element = nodes.GetArrayElementAtIndex(index);
                SerializedProperty nodeType = element.FindPropertyRelative("nodeType");
                SerializedProperty comment = element.FindPropertyRelative("comment");

                SerializedProperty emotion = element.FindPropertyRelative("emotion");
                SerializedProperty text = element.FindPropertyRelative("text");
                SerializedProperty isPlayer = element.FindPropertyRelative("isPlayer");
                SerializedProperty eventTrigger = element.FindPropertyRelative("eventTrigger");
                SerializedProperty evidence = element.FindPropertyRelative("evidence");

                rect.y += 5;
                rect.height = singleLine;

                List<Rect[]> horizontalRects = new List<Rect[]>();
                horizontalRects.Add(GetHorizontalRects(rect, new float[] { 42, 40, 9, 9 }, 5));
                horizontalRects.Add(GetHorizontalRects(rect, new float[] { 3, 2, 90 }, 2));
                Rect[][] layoutRects = GetVerticalFromHorizontalsRects(horizontalRects.ToArray(), singleLine, padding);

                if (EditorGUI.ToggleLeft(layoutRects[0][2], "+", false))
                    InsertNode(index);
                if (EditorGUI.ToggleLeft(layoutRects[0][3], "-", false))
                {
                    RemoveNode(index);
                    return;
                }

                GUIStyle style = new GUIStyle(EditorStyles.toolbarButton);
                //style = new GUIStyle(EditorStyles.toolbarDropDown);
                Color labelColor = DialogueEditorInterface.GetLabelColor((NodeType)nodeType.enumValueIndex);
                style.normal.textColor = labelColor;
                style.hover.textColor = labelColor * 1.1f;
                style.focused.textColor = labelColor * 1.1f;
                nodeType.enumValueIndex = (int)(NodeType)EditorGUI.EnumPopup(layoutRects[0][0], (NodeType)nodeType.enumValueIndex, style);
                style = EditorStyles.helpBox;
                if (EditorPrefs.GetBool("ShowStoryNodeComments"))
                    comment.stringValue = EditorGUI.TextField(layoutRects[1][2], comment.stringValue, style);
                else if (comment.stringValue != "")
                    EditorGUI.LabelField(layoutRects[1][2], "!", GUIStyle.none);

                int currentRow = 2;

                if (nodeType.enumValueIndex == (int)NodeType.Dialogue)
                {
                    element.isExpanded = EditorGUI.Foldout(layoutRects[1][1], element.isExpanded, "");
                    //style = new GUIStyle(EditorStyles.toolbarDropDown);
                    //style.normal.textColor = CharacterDefs.Instance.GetColor((CharacterName)character.enumValueIndex);
                    //character.enumValueIndex = (int)(CharacterName)EditorGUI.EnumPopup(layoutRects[0][1], (CharacterName)character.enumValueIndex, style);
                    emotion.enumValueIndex = (int)(EmotionType)EditorGUI.EnumPopup(layoutRects[0][1], (EmotionType)emotion.enumValueIndex);
                    if (element.isExpanded)
                    {
                        horizontalRects.Add(GetHorizontalRects(rect, new float[] { indentPadding, 90 }, 5));
                        horizontalRects.Add(GetHorizontalRects(rect, new float[] { indentPadding, 90 }, 5));
                        //horizontalRects.Add(GetHorizontalRects(rect, new float[] { indentPadding, 90 }, 5));
                        //horizontalRects.Add(GetHorizontalRects(rect, new float[] { indentPadding, 90 }, 5));                    
                        layoutRects = GetVerticalFromHorizontalsRects(horizontalRects.ToArray(), singleLine, padding);

                        isPlayer.boolValue = EditorGUI.Toggle(layoutRects[currentRow + 0][1], "Is Player", isPlayer.boolValue);
                        layoutRects[currentRow + 1][1].height *= 4;
                        OffsetRectsVertically(layoutRects, currentRow + 2, singleLine * 3);
                        GUIStyle textStyle = EditorStyles.textArea;
                        textStyle.wordWrap = true;
                        text.stringValue = EditorGUI.TextArea(layoutRects[currentRow + 1][1], text.stringValue, textStyle);
                    }
                }

                else if (nodeType.enumValueIndex == (int)NodeType.EventTrigger)
                {
                    element.isExpanded = EditorGUI.Foldout(layoutRects[1][1], element.isExpanded, "");
                    eventTrigger.enumValueIndex = (int)(Trigger)EditorGUI.EnumPopup(layoutRects[0][1], (Trigger)eventTrigger.enumValueIndex);
                }

                else if (nodeType.enumValueIndex == (int)NodeType.AddEvidence)
                {
                    element.isExpanded = EditorGUI.Foldout(layoutRects[1][1], element.isExpanded, "");
                    evidence.enumValueIndex = (int)(EvidenceType)EditorGUI.EnumPopup(layoutRects[0][1], (EvidenceType)evidence.enumValueIndex);
                }

                //if (nodeType.enumValueIndex == (int)NodeType.Emotion)
                //{
                //    element.isExpanded = EditorGUI.Foldout(layoutRects[1][1], element.isExpanded, "");
                //    style = new GUIStyle(EditorStyles.toolbarDropDown);
                //    style.normal.textColor = CharacterDefs.Instance.GetColor((CharacterName)character.enumValueIndex);
                //    character.enumValueIndex = (int)(CharacterName)EditorGUI.EnumPopup(layoutRects[0][1], (CharacterName)character.enumValueIndex, style);
                //    if (element.isExpanded)
                //    {
                //        horizontalRects.Add(GetHorizontalRects(rect, new float[] { indentPadding, 90 }, 5));
                //        layoutRects = GetVerticalFromHorizontalsRects(horizontalRects.ToArray(), singleLine, padding);

                //        emotion.enumValueIndex = (int)(EmotionType)EditorGUI.EnumPopup(layoutRects[currentRow + 0][1], "Emotion", (EmotionType)emotion.enumValueIndex);
                //    }
                //}

                //if (nodeType.enumValueIndex == (int)NodeType.Wait)
                //{
                //    //element.isExpanded = EditorGUI.Foldout(layoutRects[1][1], element.isExpanded, "");
                //    seconds.floatValue = EditorGUI.FloatField(layoutRects[0][1], seconds.floatValue);
                //}            

                //if (nodeType.enumValueIndex == (int)NodeType.Sfx)
                //{
                //    element.isExpanded = EditorGUI.Foldout(layoutRects[1][1], element.isExpanded, "");
                //    horizontalRects.Add(GetHorizontalRects(rect, new float[] { indentPadding, 90 }, 5));
                //    horizontalRects.Add(GetHorizontalRects(rect, new float[] { indentPadding, 90 }, 5));
                //    horizontalRects.Add(GetHorizontalRects(rect, new float[] { indentPadding, 90 }, 5));
                //    horizontalRects.Add(GetHorizontalRects(rect, new float[] { indentPadding, 90 }, 5));
                //    layoutRects = GetVerticalFromHorizontalsRects(horizontalRects.ToArray(), singleLine, padding);

                //    sfx.intValue = (int)(Sfx)EditorGUI.EnumPopup(layoutRects[0][1], (Sfx)sfx.intValue);
                //    if (element.isExpanded)
                //    {
                //        playback.enumValueIndex = (int)(Playback)EditorGUI.EnumPopup(layoutRects[currentRow][1], "Playback", (Playback)playback.enumValueIndex);
                //        seconds.floatValue = EditorGUI.FloatField(layoutRects[currentRow + 1][1], "Delay", seconds.floatValue);
                //        volume.floatValue = EditorGUI.FloatField(layoutRects[currentRow + 2][1], "Volume", volume.floatValue);
                //        waitToContinue.boolValue = EditorGUI.Toggle(layoutRects[currentRow + 3][1], "Wait To Continue", waitToContinue.boolValue);
                //    }
                //}            

                serializedObject.ApplyModifiedProperties();
            };
        }
        #endregion

        Texture2D CreateTexture(int width, int height, Color color)
        {
            Texture2D texture = new Texture2D(width, height);
            Color[] colors = new Color[width * height];
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = color;
            }
            texture.SetPixels(colors);
            texture.Apply();
            return texture;
        }

        void InsertNode(int index)
        {
            serializedObject.FindProperty("nodes").InsertArrayElementAtIndex(index);
            serializedObject.ApplyModifiedProperties();
        }

        void RemoveNode(int index)
        {
            serializedObject.FindProperty("nodes").DeleteArrayElementAtIndex(index);
            serializedObject.ApplyModifiedProperties();
        }
    }

#endif