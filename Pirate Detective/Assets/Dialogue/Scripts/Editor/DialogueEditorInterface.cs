using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Dialogue;

[CreateAssetMenu(fileName = "DialogueEditorInterface ", menuName = "Pirate Detective/Dialogue/DialogueEditorInterface ")]
public class DialogueEditorInterface : ScriptableObject
{
    public const string RESOURCES_FOLDER = "Dialogue/";

    public static DialogueEditorInterface Instance { get { if (_instance == null) _instance = Resources.Load<DialogueEditorInterface>(RESOURCES_FOLDER + typeof(DialogueEditorInterface).ToString()); return _instance; } }
    static DialogueEditorInterface _instance;

    [System.Serializable]
    public class NodeLabelColor
    {
        public NodeType node;
        public Color color = Color.white;
    }

    public List<NodeLabelColor> nodeLabelColors = new List<NodeLabelColor>();

    public static Color GetLabelColor(NodeType nodeType)
    {
        foreach(NodeLabelColor labelColor in Instance.nodeLabelColors)
        {
            if (labelColor.node == nodeType)
                return labelColor.color;
        }
        return Color.white;
    }
}
