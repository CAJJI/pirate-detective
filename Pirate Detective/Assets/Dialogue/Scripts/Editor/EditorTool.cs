﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
#if UNITY_EDITOR
//A base script that makes customizing layouts for editor tools easier.
public class EditorTool : Editor
{
    public static float singleLine { get { return EditorGUIUtility.singleLineHeight; } }

    public void BeginVertical(Action action, GUIStyle style = null)
    {
        GUILayout.BeginVertical(EditorStyles.helpBox);
        action();
        GUILayout.EndHorizontal();
    }

    public void BeginHorizontal(Action action, GUIStyle style = null)
    {
        if (style == null) style = EditorStyles.helpBox;
        GUILayout.BeginHorizontal(style);
        action();
        GUILayout.EndHorizontal();
    }

    public bool CreateButton(string label, Color color, float width = 0)
    {
        GUI.backgroundColor = color;
        bool state = false;
        if (width > 0)
            state = GUILayout.Button(label, GUILayout.Width(width));
        else
            state = GUILayout.Button(label);

        GUI.backgroundColor = Color.white;
        return state;
    }

    public static string GetOptionByNumber(int number, List<string> list)
    {
        if (number < 0 || number >= list.Count) return "";
        return list[number];
    }

    public static int GetOptionByName(string name, string[] options)
    {
        for (int i = 0; i < options.Length; i++)
        {
            if (name == options[i])
                return i;
        }
        return 1;
    }    

    #region Rect Helpers
    public static Rect[] GetHorizontalRects(Rect rect, float[] widthPercentages, float padding = 0)
    {
        Rect[] rects = new Rect[widthPercentages.Length];

        float offset = 0;
        float paddingMultiplier = 0.5f;
        for (int i = 0; i < rects.Length; i++)
        {
            rects[i] = new Rect(rect.x, rect.y, rect.width, rect.height);
            float width = (rect.width * (widthPercentages[i] / 100));
            if (i < rects.Length - 1) width -= (padding * paddingMultiplier);
            else width -= padding;
            rects[i].width = width;
            rects[i].x += offset;
            offset += width + padding;
            paddingMultiplier += 0.5f;
        }
        return rects;
    }

    public static Rect[] GetVerticalRects(Rect rect, int length, float padding = 0)
    {
        Rect[] rects = new Rect[length];

        float offset = 0;
        for (int i = 0; i < rects.Length; i++)
        {
            rects[i] = new Rect(rect.x, rect.y, rect.width, rect.height);
            rects[i].y += offset;
            offset += rect.height + padding;
        }
        return rects;
    }

    public static Rect[][] GetVerticalFromHorizontalsRects(Rect[][] horizontals, float height = 0, float padding = 0)
    {
        List<Rect[]> list = new List<Rect[]>();

        float offset = 0;
        foreach (Rect[] horizontal in horizontals)
        {
            List<Rect> rects = new List<Rect>();
            foreach (Rect rect in horizontal)
            {                
                Rect newRect = new Rect(rect.x, rect.y, rect.width, rect.height);
                newRect.y += offset;
                rects.Add(newRect);
            }
            list.Add(rects.ToArray());
            if (height == 0)
                offset += horizontal[0].height + padding;
            else                
                offset += height + padding;
        }

        return list.ToArray();
    }

    public static Rect[][] OffsetRectsVertically(Rect[][] rects, int startAtIndex, float offset)
    {
        for (int i = startAtIndex; i < rects.Length; i++)
        {
            for (int y = 0; y < rects[i].Length; y++)
            {
                rects[i][y].y += offset;
            }
        }
        return rects;
    }

    public static float GetHeightOfRects(Rect[][] rects)
    {
        float height = 0;
        foreach (Rect[] vertical in rects)
        {
            Rect horizontal = vertical[0];

            height += horizontal.y;
            height -= rects[0][0].y;
        }
        height += rects[rects.Length - 1][0].height;        
        return height;
    }

    public static Rect[] GetRectsTo2D(Rect[][] rects)
    {
        List<Rect> returnRects = new List<Rect>();

        for(int i = 0; i < rects.Length; i++)
        {
            for (int y = 0; y < rects[i].Length; y++)
            {
                returnRects.Add(rects[i][y]);
            }
        }
        return returnRects.ToArray();
    }

    List<Rect[]> NormalizeRectHeights(Rect[][] rects, float height)
    {
        for (int i = 0; i < rects.Length; i++)
        {
            NormalizeRectHeights(rects[i], height);
        }
        List<Rect[]> list = new List<Rect[]>();
        list.AddRange(rects);
        return list;
    }

    void NormalizeRectHeights(Rect[] rects, float height)
    {
        for (int i = 0; i < rects.Length; i++)
        {
            rects[i].height = height;
        }
    }
    #endregion
}
#endif