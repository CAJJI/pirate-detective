using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Dialogue;

[System.Serializable]
public class CharacterDialoguePath
{
    public bool ignore;
    public DialogueFile dialogueFile;
    public bool presentingEvidence;
    public EvidenceType evidencePresented;
    public List<Trigger> requiredTriggers = new List<Trigger>();
}

[CreateAssetMenu(fileName = "CharacterDialogueFile", menuName = "Pirate Detective/Dialogue/CharacterDialogueFile")]
public class CharacterDialogueFile : ScriptableObject
{
    public SpeechNote rootSpeechNote;
    public List<CharacterDialoguePath> dialoguePaths = new List<CharacterDialoguePath>();

    public DialogueFile GetDialogue()
    {
        return GetDialogue(false, EvidenceType.PawPrint);
    }

    public DialogueFile GetDialogue(EvidenceType evidence)
    {
        return GetDialogue(true, evidence);
    }

    DialogueFile GetDialogue(bool presentingEvidence, EvidenceType evidence)
    {
        foreach (CharacterDialoguePath path in dialoguePaths)
        {
            if (path.ignore) continue;
            if (!presentingEvidence || path.presentingEvidence)
            {
                bool skip = false;
                foreach(Trigger trigger in path.requiredTriggers)
                {
                    if (!DialogueManager.GetTrigger(trigger).triggered)
                    {
                        skip = true;
                    }
                }
                if (!skip)
                {
                    return path.dialogueFile;
                }
            }
        }
        return null;
    }
}
