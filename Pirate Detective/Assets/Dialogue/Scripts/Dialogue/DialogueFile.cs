using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Dialogue;

[CreateAssetMenu(fileName = "DialogueFile", menuName = "Pirate Detective/Dialogue/DialogueFile")]
public class DialogueFile : ScriptableObject
{    
    public string comment;
    public List<Node> nodes = new List<Node>();    

    public void AddNode(Node node)
    {
        nodes.Add(node);
    }    
}
