﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace Dialogue
    public enum NodeType
    {
        Dialogue,
        EventTrigger,
        AddEvidence,
        //Have the item itself update when an event is triggered
        //PickUp,    
    }

    public enum EmotionType
    {
        Neutral,

    }

    public enum Trigger
    {
        ToldAboutPawPrints,
    }

    public enum EvidenceType
    {
        Any,
        PawPrint,
    }


    [System.Serializable]
    public class Node
    {
        public NodeType nodeType = NodeType.Dialogue;
        public EmotionType emotion;
        public Trigger eventTrigger;
        public EvidenceType evidence;
        public string comment;
        public string text;
        public bool isPlayer;
    }