using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Dialogue;

public class DialogueEventTriggerState
{
    public Trigger eventTrigger;
    public bool triggered;

    public DialogueEventTriggerState(Trigger eventTrigger)
    {
        this.eventTrigger = eventTrigger;
    }
}

public class DialogueManager : Singleton<DialogueManager>
{    
    public List<DialogueEventTriggerState> eventTriggerStates = new List<DialogueEventTriggerState>();    

    public DialogueBox dialogueBox;
    //for testing, take out when interactables are implemented
    public CharacterDialogueFile characterDialogue;

    public Dictionary<NodeType, Interpreter> interpreters = new Dictionary<NodeType, Interpreter>();
    Interpreter activeInterpreter;
    DialogueFile activeDialogue;
    int currentNode;

    //change this when evidence inventory is added
    public List<EvidenceType> collectedEvidence;

    private void Awake()
    {
        Initialize();
    }

    public void Initialize()
    {        
        Trigger[] triggers = System.Enum.GetValues(typeof(Trigger)) as Trigger[];
        for (int i = 0; i < triggers.Length; i++)
        {
            eventTriggerStates.Add(new DialogueEventTriggerState(triggers[i]));
        }

        interpreters.Clear();
        interpreters.Add(NodeType.Dialogue, new DialogueInterpreter());
        interpreters.Add(NodeType.EventTrigger, new EventTriggerInterpreter());
        interpreters.Add(NodeType.AddEvidence, new AddEvidenceInterpreter());

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            //talk to cat
            StartDialogue(characterDialogue);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            //present evidence
            PresentEvidence(characterDialogue, EvidenceType.PawPrint);
        }

        if (Input.GetMouseButtonDown(0))
        {
            TryContinueDialogue();
        }
    }

    public void TryContinueDialogue()
    {
        if (activeDialogue == null)
            return;

        if (activeInterpreter != null)
        {
            if (activeInterpreter.CanContinue())
            {
                activeInterpreter.OnContinue();
            }
        }
        else
        {
            ContinueDialogue();
        }
    }

    public void ContinueDialogue()
    {
        Debug.Log(currentNode + " : " + activeDialogue.nodes.Count);

        if (activeInterpreter != null)
        {
            currentNode++;
        }

        if (currentNode >= activeDialogue.nodes.Count)
        {
            EndDialogue();
            return;
        }        

        ParseNode(activeDialogue.nodes[currentNode]);        
    }

    void ParseNode(Node node)
    {
        activeInterpreter = interpreters[node.nodeType];
        Debug.Log(activeInterpreter.ToString());
        activeInterpreter.InterpretNode(node);
    }

    public void StartDialogue(CharacterDialogueFile characterDialogueFile)
    {
        SetupDialogue(characterDialogueFile.GetDialogue(), characterDialogueFile);
    }

    public void PresentEvidence(CharacterDialogueFile characterDialogueFile, EvidenceType evidence)
    {
        if (!collectedEvidence.Contains(evidence))
            Debug.Log("dont have evidence");
        else
        SetupDialogue(characterDialogueFile.GetDialogue(evidence), characterDialogueFile);
    }

    public void SetupDialogue(DialogueFile dialogueFile, CharacterDialogueFile characterFile)
    {
        currentNode = 0;
        activeDialogue = dialogueFile;
        DialogueBox.Instance.SetSpeaker(characterFile);
        ContinueDialogue();
    }

    public void EndDialogue()
    {
        Debug.Log("End Dialogue");
        currentNode = 0;
        activeInterpreter = null;
        activeDialogue = null;
        dialogueBox.ClearText();
    }

    public static DialogueEventTriggerState GetTrigger(Trigger trigger)
    {
        foreach (DialogueEventTriggerState triggerState in Instance.eventTriggerStates)
        {
            if (triggerState.eventTrigger == trigger)
                return triggerState;
        }
        return null;
    }

    public static void AddEvidence(EvidenceType evidence)
    {
        if (!Instance.collectedEvidence.Contains(evidence))
        {
            Instance.collectedEvidence.Add(evidence);
        }
    }
}
