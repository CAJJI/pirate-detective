﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

//namespace Dialogue

public class DialogueInterpreter : Interpreter
{
    public override void OnContinue()
    {
        if (DialogueBox.Instance.isTypingText)
        {
            DialogueBox.Instance.CompleteTypingText();
            return;
        }

        else
        {
            ProgressDialogue();
        }
    }

    public override void InterpretNode(Node node)
    {
        base.InterpretNode(node);
        DialogueBox.Instance.SetIsPlayer(node.isPlayer);
        DialogueBox.Instance.SetText(node.text);
    }
}
