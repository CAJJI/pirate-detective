using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace Dialogue

    public class AddEvidenceInterpreter : Interpreter
    {
        public override void InterpretNode(Node node)
        {
            base.InterpretNode(node);
            //add evidence
            DialogueManager.AddEvidence(node.evidence);
            ProgressDialogue();
        }
    }
