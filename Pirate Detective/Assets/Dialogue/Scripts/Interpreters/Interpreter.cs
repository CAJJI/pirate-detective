﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace Dialogue

    public class Interpreter
    {
        public Node activeNode;

        public virtual void Update()
        {

        }

        public virtual bool CanContinue()
        {
            return true;
        }

        public virtual void OnContinue()
        {
            ProgressDialogue();
        }

        public virtual void InterpretNode(Node node)
        {
            activeNode = node;
        }

        public void ProgressDialogue()
        {            
            DialogueManager.Instance.ContinueDialogue();
        }
    }
